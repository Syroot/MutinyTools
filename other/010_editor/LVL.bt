//------------------------------------------------
//--- 010 Editor v8.0 Binary Template
//
//      File: LVL.bt
//   Authors: Syroot
//   Version: 1.0.0
//   Purpose: Parse Mutiny of the Things level files.
//  Category: MOTT
// File Mask: *.lvl
//  ID Bytes: 
//   History: 
// 	1.0.0	2017-08-04	Initial version.
//
// For simplicity, all structures are forward declared first, then the definitions imported from files.
//------------------------------------------------

// ==== Forward Declarations ===========================================================================================

struct Level;
struct Player;
struct Apple;
struct Carrot;
struct Daisy;
struct Chopper;
struct Shoe;
struct Pencil;
struct Ooze;
struct Tank;
struct Snowman;
struct IceCone;
struct FlatIron;
struct Lightbulb;
struct Elevator;
struct Stomper;

// ==== Structures =====================================================================================================

typedef struct // Level
{
	enum <byte> LevelWorld
	{
		LW_None,
		LW_Factory,
		LW_Nature,
		LW_Ice,
		LW_Space
	} world;
	// Read the tileset file names.
	byte foreFileLength;
	char foreFile[foreFileLength];
	Padding(0xFF - foreFileLength);
	byte backFileLength;
	char backFile[backFileLength];
	Padding(0xFF - backFileLength);
	byte machineFileLength;
	char machineFile[machineFileLength];
	Padding(0xFF - machineFileLength);
	// Read the layer data.
	byte foreLayer[0x80 * 0x60] <bgcolor=0xBDBDEB>;
	byte itemLayer[0x80 * 0x60] <bgcolor=0xAEAED9>;
	byte backLayer[0x40 * 0x30] <bgcolor=0xA1A1C8>;
	// Read actors.
	Player player;
	Apple apples[10];
	Carrot carrots[10];
	Daisy daisies[10];
	Chopper choppers[10];
	Shoe shoes[10];
	Pencil pencils[10];
	Ooze oozes[10];
	Tank tanks[10];
	Snowman snowmen[10];
	IceCone iceCones[10];
	FlatIron flatIrons[10];
	Lightbulb lightbulbs[10];
	Elevator elevators[10];
	Stomper stompers[10];
} Level <bgcolor=0xCDCDFF>;

typedef struct // Player
{
	short score; // Overwritten after load.
	short x;
	short y;
	short ySpeed;
	short xSpeed; // -8 or 8 for walk speed
	short timer; // Used to measure invincibility time
	byte health; // Overwritten after load.
	byte ammunition; // Overwritten after load.
	byte unknown0x08; // Always 0?
	byte sprite;
	byte jumpProgress; // 0-9
	byte direction; // 1 = left JJ, 2 = right JJ
	byte walkingRight;
	byte walkingLeft;
	byte shooting; // bool
	byte jumping; // bool
	byte falling; // bool
	byte bouncing; // bool, unused in release including sound
	byte stunned; // bool
	byte yawning; // bool
	byte hurt; // bool
	byte unknown0x19; // Always 0?
	byte invincible; // bool
	byte unknown0x1D;
} Player <bgcolor=0xEFCDFF>;

typedef struct // Apple
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
} Apple <bgcolor=0x7171FF>;

typedef struct // Carrot
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
} Carrot <bgcolor=0x0099FF>;

typedef struct // Daisy
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
	byte unknown1;
	byte firing;
} Daisy <bgcolor=0xDDDDDD>;

typedef struct // Chopper
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
} Chopper <bgcolor=0x5E5EFF>;

typedef struct // Shoe
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
} Shoe <bgcolor=0xAEAED9>;

typedef struct // Pencil
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
	byte unknown1; // Never written, unused?
	byte firing;
} Pencil <bgcolor=0xCDFFCD>;

typedef struct // Ooze
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
} Ooze <bgcolor=0x95B895>;

typedef struct // Tank
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
	byte unknown1; // Never written, unused?
	byte firing;
	byte unknown2;
} Tank <bgcolor=0xAAAAAA>;

typedef struct // Snowman
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
} Snowman <bgcolor=0xEEEEEE>;

typedef struct // IceCone
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
} IceCone <bgcolor=0xFFCDCD>;

typedef struct // FlatIron
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
} FlatIron <bgcolor=0xCCCCCC>;

typedef struct // Lightbulb
{
	short x;
	short y;
	byte health;
	byte direction;
	byte sprite;
} Lightbulb <bgcolor=0x00FFFF>;

typedef struct // Elevator
{
	short x;
	short y;
	byte direction; // 1 = up, other = down
	byte health;
} Elevator <bgcolor=0xCDFFEF>;

typedef struct // Stomper
{
	short x;
	short y;
	byte health;
	byte state;
} Stomper <bgcolor=0xFFEFCD>;

// ==== File Contents ==================================================================================================

BitfieldRightToLeft();
Level level <open=true>;

// ==== Template Methods ===============================================================================================

void FAlign(int alignment)
{
	Padding((-FTell() % alignment + alignment) % alignment);
}

void Padding(int count)
{
	while (count--)
	{
		byte padding <fgcolor=0x808080, hidden=true>;
	}
}
