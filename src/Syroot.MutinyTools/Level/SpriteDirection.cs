﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Syroot.MutinyTools.Level
{
    public enum SpriteDirection : byte
    {
        None,
        LeftOrUp,
        RightOrDown
    }
}
