﻿using System.Runtime.InteropServices;

namespace Syroot.MutinyTools.Level
{
    public interface IActor
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        short X { get; set; }
        short Y { get; set; }
        SpriteDirection Direction { get; set; }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Player : IActor
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public short Score { get; set; } // Overwritten
        public short X { get; set; }
        public short Y { get; set; }
        public short YSpeed { get; set; }
        public short XSpeed { get; set; }
        public short Timer { get; set; }
        public byte Health { get; set; } // Overwritten
        public byte Ammunition { get; set; } // Overwritten
        public byte Unknown0x08 { get; set; }
        public byte Sprite { get; set; }
        public byte JumpProgress { get; set; }
        public SpriteDirection Direction { get; set; }
        public bool WalkingRight { get; set; }
        public bool WalkingLeft { get; set; }
        public bool Shooting { get; set; }
        public bool Jumping { get; set; }
        public bool Falling { get; set; }
        public bool Bouncing { get; set; } // Unused
        public bool Stunned { get; set; }
        public bool Yawning { get; set; }
        public bool Hurt { get; set; }
        public bool Unknown0x19 { get; set; }
        public bool Invincible { get; set; }
        public bool unknown0x1D { get; set; }
    }

    /// <summary>
    /// Represents Apple, Carrot, Chopper, Shoe, Ooze, Snowman, Ice Cone and Lightbulb enemies.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct WalkingEnemy : IActor
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public short X { get; set; }
        public short Y { get; set; }
        public byte Health { get; set; }
        public SpriteDirection Direction { get; set; }
        public byte Sprite { get; set; }
    }

    /// <summary>
    /// Represents Daisy and Pencil enemies.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ShootingEnemy : IActor
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public short X { get; set; }
        public short Y { get; set; }
        public byte Health { get; set; }
        public SpriteDirection Direction { get; set; }
        public byte Sprite { get; set; }
        public byte Unknown0x07 { get; set; }
        public bool Firing { get; set; }
    }

    /// <summary>
    /// Represents Tank enemies.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ShootingEnemyEx : IActor
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public short X { get; set; }
        public short Y { get; set; }
        public byte Health { get; set; }
        public SpriteDirection Direction { get; set; }
        public byte Sprite { get; set; }
        public byte Unknown0x07 { get; set; }
        public bool Firing { get; set; }
        public bool Unknown0x09 { get; set; }
    }

    /// <summary>
    /// Represents elevators.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Elevator : IActor
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public short X { get; set; }
        public short Y { get; set; }
        public SpriteDirection Direction { get; set; }
        public byte Health { get; set; }
    }

    /// <summary>
    /// Represents stompers.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Stomper : IActor
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public short X { get; set; }
        public short Y { get; set; }
        public SpriteDirection Direction { get; set; }
        public byte State { get; set; }
    }
}
