﻿namespace Syroot.MutinyTools.Level
{
    public enum ActorType
    {
        Player,
        Apple,
        Carrot,
        Daisy,
        Chopper,
        Shoe,
        Pencil,
        Ooze,
        Tank,
        Snowman,
        IceCone,
        FlatIron,
        Lightbulb,
        Elevator,
        Stomper
    }
}
