﻿using System;
using System.IO;
using Syroot.BinaryData;
using Syroot.Maths;
using Syroot.MutinyTools.Core;

namespace Syroot.MutinyTools.Level
{
    /// <summary>
    /// Represents a Mutiny of the Things level file.
    /// </summary>
    public class LevelFile
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        public static readonly Vector2 TileLayerSize = new Vector2(0x80, 0x60);
        public static readonly Vector2 BackTileLayerSize = new Vector2(0x40, 0x30);
        public const int ActorCount = 10;

        private const int _tilesetNameSize = 0xFF;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="LevelFile"/> class.
        /// </summary>
        public LevelFile()
        {
            World = LevelWorld.Industrial;
            TilesetFileName = "for-fab.pic";
            BackTilesetFileName = "hin-fab.pic";
            MachineTilesetFileName = "strom.pic";
            Tiles = new byte[TileLayerSize.X * TileLayerSize.Y];
            ItemTiles = new byte[TileLayerSize.X * TileLayerSize.Y];
            BackTiles = new byte[BackTileLayerSize.X * BackTileLayerSize.Y];
            Player = new Player();
            Apples = new WalkingEnemy[ActorCount];
            Carrots = new WalkingEnemy[ActorCount];
            Daisies = new ShootingEnemy[ActorCount];
            Choppers = new WalkingEnemy[ActorCount];
            Shoes = new WalkingEnemy[ActorCount];
            Pencils = new ShootingEnemy[ActorCount];
            Oozes = new WalkingEnemy[ActorCount];
            Tanks = new ShootingEnemyEx[ActorCount];
            Snowmen = new WalkingEnemy[ActorCount];
            IceCones = new WalkingEnemy[ActorCount];
            FlatIrons = new WalkingEnemy[ActorCount];
            Lightbulbs = new WalkingEnemy[ActorCount];
            Elevators = new Elevator[ActorCount];
            Stompers = new Stomper[ActorCount];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LevelFile"/> class from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the level from.</param>
        public LevelFile(string fileName)
        {
            Load(fileName);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LevelFile"/> class from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> of the file to load the level from.</param>
        public LevelFile(Stream stream, bool leaveOpen = false)
        {
            Load(stream, leaveOpen);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public LevelWorld World { get; set; }

        public string TilesetFileName { get; set; }

        public string BackTilesetFileName { get; set; }

        public string MachineTilesetFileName { get; set; }

        public byte[] Tiles { get; set; }

        public byte[] ItemTiles { get; set; }

        public byte[] BackTiles { get; set; }

        public Player Player { get; set; }

        public WalkingEnemy[] Apples { get; set; }

        public WalkingEnemy[] Carrots { get; set; }

        public ShootingEnemy[] Daisies { get; set; }

        public WalkingEnemy[] Choppers { get; set; }

        public WalkingEnemy[] Shoes { get; set; }

        public ShootingEnemy[] Pencils { get; set; }

        public WalkingEnemy[] Oozes { get; set; }

        public ShootingEnemyEx[] Tanks { get; set; }

        public WalkingEnemy[] Snowmen { get; set; }

        public WalkingEnemy[] IceCones { get; set; }

        public WalkingEnemy[] FlatIrons { get; set; }

        public WalkingEnemy[] Lightbulbs { get; set; }

        public Elevator[] Elevators { get; set; }

        public Stomper[] Stompers { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the instance from the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the instance from.</param>
        public void Load(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                Load(stream);
            }
        }

        /// <summary>
        /// Loads the instance from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the instance from.</param>
        public void Load(Stream stream, bool leaveOpen = false)
        {
            using (BinaryDataReader reader = new BinaryDataReader(stream, leaveOpen))
            {
                World = reader.ReadEnum<LevelWorld>(true);
                TilesetFileName = reader.ReadBlockString(_tilesetNameSize);
                BackTilesetFileName = reader.ReadBlockString(_tilesetNameSize);
                MachineTilesetFileName = reader.ReadBlockString(_tilesetNameSize);
                Tiles = reader.ReadBytes(TileLayerSize.X * TileLayerSize.Y);
                ItemTiles = reader.ReadBytes(TileLayerSize.X * TileLayerSize.Y);
                BackTiles = reader.ReadBytes(BackTileLayerSize.X * BackTileLayerSize.Y);
                Player = reader.ReadObject<Player>();
                Apples = reader.ReadObjects<WalkingEnemy>(ActorCount);
                Carrots = reader.ReadObjects<WalkingEnemy>(ActorCount);
                Daisies = reader.ReadObjects<ShootingEnemy>(ActorCount);
                Choppers = reader.ReadObjects<WalkingEnemy>(ActorCount);
                Shoes = reader.ReadObjects<WalkingEnemy>(ActorCount);
                Pencils = reader.ReadObjects<ShootingEnemy>(ActorCount);
                Oozes = reader.ReadObjects<WalkingEnemy>(ActorCount);
                Tanks = reader.ReadObjects<ShootingEnemyEx>(ActorCount);
                Snowmen = reader.ReadObjects<WalkingEnemy>(ActorCount);
                IceCones = reader.ReadObjects<WalkingEnemy>(ActorCount);
                FlatIrons = reader.ReadObjects<WalkingEnemy>(ActorCount);
                Lightbulbs = reader.ReadObjects<WalkingEnemy>(ActorCount);
                Elevators = reader.ReadObjects<Elevator>(ActorCount);
                Stompers = reader.ReadObjects<Stomper>(ActorCount);
            }
        }

        /// <summary>
        /// Saves the level in the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to level the bitmap in.</param>
        public void Save(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                Save(stream);
            }
        }

        /// <summary>
        /// Saves the level in the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to store the level in.</param>
        public void Save(Stream stream, bool leaveOpen = false)
        {
            using (BinaryDataWriter writer = new BinaryDataWriter(stream, leaveOpen))
            {
                writer.Write(World, true);
                writer.WriteBlockString(TilesetFileName, _tilesetNameSize);
                writer.WriteBlockString(BackTilesetFileName, _tilesetNameSize);
                writer.WriteBlockString(MachineTilesetFileName, _tilesetNameSize);
                writer.Write(Tiles);
                writer.Write(ItemTiles);
                writer.Write(BackTiles);
                writer.WriteObject(Player);
                writer.WriteObject(Apples);
                writer.WriteObject(Carrots);
                writer.WriteObject(Daisies);
                writer.WriteObject(Choppers);
                writer.WriteObject(Shoes);
                writer.WriteObject(Pencils);
                writer.WriteObject(Oozes);
                writer.WriteObject(Tanks);
                writer.WriteObject(Snowmen);
                writer.WriteObject(IceCones);
                writer.WriteObject(FlatIrons);
                writer.WriteObject(Lightbulbs);
                writer.WriteObject(Elevators);
                writer.WriteObject(Stompers);
            }
        }
    }
}
