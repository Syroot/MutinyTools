﻿namespace Syroot.MutinyTools.Level
{
    public enum LevelWorld : byte
    {
        Invalid,
        Industrial,
        Wilderness,
        Ice,
        Space
    }
}
