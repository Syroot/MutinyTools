﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Syroot.BinaryData;
using Syroot.MutinyTools.Core;

namespace Syroot.MutinyTools.Container
{
    /// <summary>
    /// Represents a data container file which stores several different files in itself, referenced through a table of
    /// contents at the beginning of the container file.
    /// </summary>
    public class ContainerFile : IDisposable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const string _header = "CAPS Container V1.0; 01.12.1993\x1A";
        private const int _entrySize = 25;
        private const int _entryNameSize = 12;

        private static readonly Encoding _encoding = CodePagesEncodingProvider.Instance.GetEncoding(850);

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainerFile"/> class.
        /// </summary>
        public ContainerFile()
        {
            Reset();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainerFile"/> class from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the container from.</param>
        public ContainerFile(string fileName)
        {
            Load(fileName);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContainerFile"/> class from the given
        /// <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> of the file to load the container from.</param>
        public ContainerFile(Stream stream, bool leaveOpen = false)
        {
            Load(stream, leaveOpen);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets the dictionary mapping file names to <see cref="IContainerEntry"/> instances retrieving the data.
        /// </summary>
        public SortedDictionary<string, IContainerEntry> Entries { get; private set; }
        
        internal BinaryDataReader Reader { get; private set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------
        
        /// <summary>
        /// Loads the instance from the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the instance from.</param>
        public void Load(string fileName)
        {
            Load(new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read));
        }

        /// <summary>
        /// Loads the instance from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the instance from.</param>
        public void Load(Stream stream, bool leaveOpen = false)
        {
            Reset();
            Reader = new BinaryDataReader(stream, _encoding, leaveOpen);

            // Read the header.
            if (Reader.ReadString(_header.Length) != _header)
            {
                throw new InvalidDataException("Invalid container header.");
            }

            // Get the number of items in this container and read their headers (e.g. the table of contents).
            int itemCount = Reader.ReadInt16();
            for (int i = 0; i < itemCount; i++)
            {
                string name = Reader.ReadBlockString(_entryNameSize);
                int length = Reader.ReadInt32();
                int startOffset = Reader.ReadInt32();
                int endOffset = Reader.ReadInt32(); // Redundant, can be computed from start offset and end offset.
                Entries.Add(name, new SourceContainerEntry(this, startOffset, length));
            }
        }

        /// <summary>
        /// Packs the content in this container and stores it in a new file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to store the container in.</param>
        public void Save(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                Save(stream);
            }
        }

        /// <summary>
        /// Packs the content in this container and writes it into the given <paramref name="stream"/>.
        /// </summary>
        ///<param name="stream">The <see cref="Stream"/> to store the container in.</param>
        public void Save(Stream stream, bool leaveOpen = false)
        {
            using (BinaryDataWriter writer = new BinaryDataWriter(stream, _encoding, leaveOpen))
            {
                // Write the header.
                writer.Write(_header, BinaryStringFormat.NoPrefixOrTermination);
                
                // Write the number of items and their headers (e.g. the table of contents).
                writer.Write((short)Entries.Count);
                int currentOffset = _header.Length + sizeof(short) + (Entries.Count * _entrySize);
                foreach (KeyValuePair<string, IContainerEntry> entry in Entries)
                {
                    writer.WriteBlockString(entry.Key, _entryNameSize);

                    // Seek to the beginning of the entries data to let it be written there, then get the next offset.
                    int entryLength;
                    using (writer.TemporarySeek(currentOffset, SeekOrigin.Begin))
                    {
                        entry.Value.CopyDataTo(writer.BaseStream);
                        entryLength = (int)writer.BaseStream.Position - currentOffset;
                    }

                    // Write the offsets into the table of contents.
                    writer.Write(entryLength);
                    writer.Write(currentOffset);
                    currentOffset += entryLength;
                    writer.Write(currentOffset);

                    writer.Flush();
                }
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Reader.Dispose();
                }
                _disposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Reset()
        {
            Entries = new SortedDictionary<string, IContainerEntry>(StringComparer.OrdinalIgnoreCase);
        }
    }
}
