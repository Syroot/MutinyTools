﻿using System.IO;

namespace Syroot.MutinyTools.Container
{
    /// <summary>
    /// Represents a <see cref="Container"/> entry which data is stored in memory.
    /// </summary>
    public class RawContainerEntry : IContainerEntry
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private byte[] _data;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="RawContainerEntry"/> class referencing the given
        /// <paramref name="data"/>.
        /// </summary>
        /// <param name="data">The data to reference.</param>
        public RawContainerEntry(byte[] data)
        {
            _data = data;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Copies the data of the entry to the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to copy the entry data to.</param>
        public void CopyDataTo(Stream stream)
        {
            using (MemoryStream memoryStream = new MemoryStream(_data, false))
            {
                memoryStream.CopyTo(stream);
            }
        }
    }
}
