﻿using System.IO;
using Syroot.MutinyTools.Core;

namespace Syroot.MutinyTools.Container
{
    /// <summary>
    /// Represents a <see cref="Container"/> entry which data is read from a <see cref="ContainerFile"/>.
    /// </summary>
    public class SourceContainerEntry : IContainerEntry
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private ContainerFile _sourceContainer;
        private int _startOffset;
        private int _length;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceContainerEntry"/> class referencing data from the given
        /// <paramref name="container"/>.
        /// </summary>
        /// <param name="container">The <see cref="ContainerFile"/> storing the data of this entry.</param>
        /// <param name="startOffset">The start offset of the entry data in the container.</param>
        /// <param name="length">The length of the entry data in bytes.</param>
        internal SourceContainerEntry(ContainerFile container, int startOffset, int length)
        {
            _sourceContainer = container;
            _startOffset = startOffset;
            _length = length;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Copies the data of the entry to the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to copy the entry data to.</param>
        public void CopyDataTo(Stream stream)
        {
            _sourceContainer.Reader.Position = _startOffset;
            _sourceContainer.Reader.CopyTo(stream, _length);
        }
    }
}
