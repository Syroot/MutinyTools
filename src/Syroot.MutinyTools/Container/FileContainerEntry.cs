﻿using System.IO;

namespace Syroot.MutinyTools.Container
{
    /// <summary>
    /// Represents a <see cref="Container"/> entry which data is read from a file.
    /// </summary>
    public class FileContainerEntry : IContainerEntry
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private string _fileName;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FileContainerEntry"/> class referencing data from the file with
        /// the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file storing the data of this entry.</param>
        public FileContainerEntry(string fileName)
        {
            _fileName = fileName;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Copies the data of the entry to the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to copy the entry data to.</param>
        public void CopyDataTo(Stream stream)
        {
            using (FileStream fileStream = new FileStream(_fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                fileStream.CopyTo(stream);
            }
        }
    }
}
