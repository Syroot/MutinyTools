﻿using System.IO;

namespace Syroot.MutinyTools.Container
{
    /// <summary>
    /// Represents the common interface of entries in a <see cref="ContainerFile"/>, which can have different data
    /// sources.
    /// </summary>
    public interface IContainerEntry
    {
        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Copies the data of the entry to the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to copy the entry data to.</param>
        void CopyDataTo(Stream stream);
    }
}
