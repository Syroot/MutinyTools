﻿using System;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.MutinyTools.Core
{
    /// <summary>
    /// Represents extension methods for the <see cref="BinaryDataReader"/> and <see cref="BinaryDataWriter"/> classes.
    /// </summary>
    internal static class BinaryDataExtensions
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _copyToBuffer = 81920;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static void CopyTo(this BinaryDataReader self, Stream stream, int count)
        {
            if (count == 0) return;
            if (count < 0) throw new ArgumentOutOfRangeException("Count must be a positive value.", nameof(count));
            if (!stream.CanWrite) throw new ArgumentException("Target stream is not writable.", nameof(stream));

            byte[] buffer = new byte[_copyToBuffer];
            while (count > 0)
            {
                int bytesToRead = Math.Min(buffer.Length, count);
                if (self.BaseStream.Read(buffer, 0, Math.Min(buffer.Length, count)) != bytesToRead)
                {
                    throw new IOException("Could not read requested number of bytes.");
                }
                stream.Write(buffer, 0, bytesToRead);
                count -= bytesToRead;
            }
        }
        
        internal static string ReadBlockString(this BinaryDataReader self, int blockLength)
        {
            // Read a whole block, but only use as many bytes as determined by the byte prefix for the string.
            string name = self.ReadString(BinaryStringFormat.ByteLengthPrefix);
            self.Position += blockLength - name.Length;
            return name;
        }
        
        internal static void WriteBlockString(this BinaryDataWriter self, string value, int blockLength)
        {
            // Trim the name to max 12 chars and convert to uppercase. Fill up to 12 zero bytes.
            value = value.Substring(0, Math.Min(value.Length, 12)).ToUpper();
            self.Write((byte)value.Length);
            self.Write(value, BinaryStringFormat.NoPrefixOrTermination);
            self.Position += blockLength - value.Length;
        }
    }
}
