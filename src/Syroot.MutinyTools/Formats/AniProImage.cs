﻿using System.IO;
using Syroot.BinaryData;
using Syroot.Maths;

namespace Syroot.MutinyTools.Formats
{
    /// <summary>
    /// Represents a graphical image in the Animation Pro image file format in CEL or PIC files.
    /// </summary>
    public class AniProImage
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const ushort _header = 0x9119;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------
        
        /// <summary>
        /// Initializes a new instance of the <see cref="AniProImage"/> class from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the bitmap from.</param>
        public AniProImage(string fileName)
        {
            Load(fileName);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AniProImage"/> class from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> of the file to load the bitmap from.</param>
        public AniProImage(Stream stream, bool leaveOpen = false)
        {
            Load(stream, leaveOpen);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets or sets the dimensions of the image <see cref="Data"/>.
        /// </summary>
        public Vector2 Size { get; set; }
        
        /// <summary>
        /// Gets or sets the coordinate of the upper left corner. This should be 0 for PIC files.
        /// </summary>
        public Vector2 Origin { get; set; }

        /// <summary>
        /// Gets or sets the color depth for each pixel. Only 8 is known.
        /// </summary>
        public byte BitsPerPixel { get; set; }

        /// <summary>
        /// Gets or sets the compression used by this image. Only <see cref="AniProImageCompression.Uncompressed"/> is
        /// known.
        /// </summary>
        public AniProImageCompression Compression { get; set; }
        
        /// <summary>
        /// Gets or sets the colors referenced by the image <see cref="Data"/>.
        /// </summary>
        public Color[] Palette { get; set; }
        
        /// <summary>
        /// Gets or sets the indices into the color palette for each pixel.
        /// </summary>
        public byte[] Data { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the instance from the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the instance from.</param>
        public void Load(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                Load(stream);
            }
        }

        /// <summary>
        /// Loads the instance from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the instance from.</param>
        public void Load(Stream stream, bool leaveOpen = false)
        {
            using (BinaryDataReader reader = new BinaryDataReader(stream, leaveOpen))
            {
                // Read and check the header.
                if (reader.ReadUInt16() != _header)
                {
                    throw new InvalidDataException("Invalid AniProImage header.");
                }

                // Get the real image size. Tilesets are stored differently and need a fix to look visually correct.
                Size = new Vector2(reader.ReadInt16(), reader.ReadInt16());

                // Origin may be set for CEL files, but is always 0 for PIC files.
                Origin = new Vector2(reader.ReadInt16(), reader.ReadInt16());

                // Bits per pixel is always 8, compression flags always uncompressed (0).
                BitsPerPixel = reader.ReadByte();
                Compression = reader.ReadEnum<AniProImageCompression>(true);

                // Data size is width * height. 8 reserved shorts follow which are set to 0.
                int dataSize = reader.ReadInt32();
                short[] reserved = reader.ReadInt16s(8);

                // Read the palette.
                Palette = new Color[256];
                for (int i = 0; i < 256; i++)
                {
                    // Colors lie in the range 0 to 63, shift them to become 0 to 255.
                    byte r = (byte)(reader.ReadByte() << 2);
                    byte g = (byte)(reader.ReadByte() << 2);
                    byte b = (byte)(reader.ReadByte() << 2);
                    Palette[i] = new Color(r, g, b);
                }

                // Store the pixel palette indices.
                Data = reader.ReadBytes(dataSize);
            }
        }

        /// <summary>
        /// Saves the bitmap in the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to store the bitmap in.</param>
        public void Save(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                Save(stream);
            }
        }

        /// <summary>
        /// Saves the bitmap in the given <paramref name="stream"/>.
        /// </summary>
        ///<param name="stream">The <see cref="Stream"/> to store the bitmap in.</param>
        public void Save(Stream stream, bool leaveOpen = false)
        {
            using (BinaryDataWriter writer = new BinaryDataWriter(stream, leaveOpen))
            {
                // Write the header.
                writer.Write(_header);
                writer.Write((short)Size.X);
                writer.Write((short)Size.Y);
                writer.Write((short)Origin.X);
                writer.Write((short)Origin.Y);
                writer.Write(BitsPerPixel);
                writer.Write(Compression, true);

                // Write the data length and the 8 reserved empty shorts.
                writer.Write(Data.Length);
                writer.Position += 8 * sizeof(short);

                // Write the palette.
                for (int i = 0; i < Palette.Length; i++)
                {
                    // Bring back the colors into a range of 0 to 63.
                    Color color = Palette[i];
                    writer.Write((byte)(color.R >> 2));
                    writer.Write((byte)(color.G >> 2));
                    writer.Write((byte)(color.B >> 2));
                }

                // Write the image data.
                writer.Write(Data);
            }
        }
    }

    /// <summary>
    /// Represents the set of known compression flags used by an <see cref="AniProBitmap"/>.
    /// </summary>
    public enum AniProImageCompression : byte
    {
        /// <summary>
        /// No compression.
        /// </summary>
        Uncompressed
    }
}
