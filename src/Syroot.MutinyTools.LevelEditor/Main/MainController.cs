﻿using System;
using System.Windows.Forms;
using Syroot.MutinyTools.Level;

namespace Syroot.MutinyTools.LevelEditor.Main
{
    internal class MainController
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private LevelFile _level;
        private LevelLayer _activeLayer;
        private bool _fadeInactiveLayers = true;
        private bool _showParallax = true;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal MainController()
        {
            GameData = new GameData();
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------
        
        internal event EventHandler LevelChanged;

        internal event EventHandler LevelWorldChanged;

        internal event EventHandler ActiveLayerChanged;

        internal event EventHandler FadeInactiveLayersChanged;

        internal event EventHandler ParallaxVisibleChanged;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal GameData GameData { get; set; }

        internal string LevelFileName { get; set; }

        internal LevelFile Level
        {
            get { return _level; }
            set
            {
                _level = value;
                LevelChanged?.Invoke(this, EventArgs.Empty);
                LevelWorldChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        internal LevelLayer ActiveLayer
        {
            get { return _activeLayer; }
            set
            {
                _activeLayer = value;
                ActiveLayerChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        internal bool FadeInactiveLayers
        {
            get { return _fadeInactiveLayers; }
            set
            {
                _fadeInactiveLayers = value;
                FadeInactiveLayersChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        internal bool ParallaxVisible
        {
            get { return _showParallax; }
            set
            {
                _showParallax = value;
                ParallaxVisibleChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void SetLevelWorld(LevelWorld world)
        {
            Level.World = world;
            LevelWorldChanged?.Invoke(this, EventArgs.Empty);
        }

        internal void ExitApplication()
        {
            Application.Exit();    
        }
    }
}
