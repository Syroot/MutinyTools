﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Syroot.MutinyTools.Level;
using Syroot.MutinyTools.LevelEditor.Main.Commands;

namespace Syroot.MutinyTools.LevelEditor.Main
{
    /// <summary>
    /// Represents the main window of the application.
    /// </summary>
    public partial class MainWindow : Form
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;
        private Dictionary<ICommand, ToolStripItem[]> _commandBindings;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="Form"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            InitializeController();
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void InitializeController()
        {
            _controller = new MainController();
            _controller.LevelChanged += _controller_LevelChanged;
            _controller.LevelWorldChanged += _controller_LevelWorldChanged;
            _controller.ActiveLayerChanged += _controller_ActiveLayerChanged;
            _controller.FadeInactiveLayersChanged += _controller_FadeInactiveLayersChanged;
            _controller.ParallaxVisibleChanged += _controller_ParallaxVisibleChanged;

            _commandBindings = new Dictionary<ICommand, ToolStripItem[]>();
            BindCommand(new NewLevelCommand(_controller), _tsmiMainFileNew, _tsbMainFileNew);
            BindCommand(new OpenLevelCommand(_controller), _tsmiMainFileOpen, _tsbMainFileOpen);
            BindCommand(new SaveLevelCommand(_controller), _tsmiMainFileSave, _tsbMainFileSave);
            BindCommand(new SaveLevelAsCommand(_controller), _tsmiMainFileSaveAs, _tsbMainFileSaveAs);
            BindCommand(new CloseLevelCommand(_controller), _tsmiMainFileClose, _tsbMainFileClose);
            BindCommand(new ExitCommand(_controller), _tsmiMainFileExit);
            BindCommand(new WorldCommand(_controller, LevelWorld.Industrial),
                _tsmiMainWorldIndustrial, _tsbMainWorldIndustrial);
            BindCommand(new WorldCommand(_controller, LevelWorld.Wilderness),
                _tsmiMainWorldWilderness, _tsbMainWorldWilderness);
            BindCommand(new WorldCommand(_controller, LevelWorld.Ice),
                _tsmiMainWorldIce, _tsbMainWorldIce);
            BindCommand(new WorldCommand(_controller, LevelWorld.Space),
                _tsmiMainWorldSpace, _tsbMainWorldSpace);
            BindCommand(new LayerCommand(_controller, LevelLayer.Background),
                _tsmiMainLayerBackground, _tsbMainLayerBackground);
            BindCommand(new LayerCommand(_controller, LevelLayer.Items),
                _tsmiMainLayerTiles, _tsbMainLayerTiles);
            BindCommand(new LayerCommand(_controller, LevelLayer.Actors),
                _tsmiMainLayerActors, _tsbMainLayerActors);
            BindCommand(new LayerCommand(_controller, LevelLayer.Foreground),
                _tsmiMainLayerForeground, _tsbMainLayerForeground);
            BindCommand(new ToggleFadeCommand(_controller), _tsmiMainViewFadeInactive, _tsbMainViewFadeInactive);
            BindCommand(new ToggleParallaxCommand(_controller), _tsmiMainViewParallax, _tsbMainViewParallax);
            BindCommand(new PackContainerCommand(_controller), _tsmiMainToolsPack, _tsbMainToolsPack);
            BindCommand(new UnpackContainerCommand(_controller), _tsmiMainToolsUnpack, _tsbMainToolsUnpack);
            UpdateCommands();
        }

        private void BindCommand(ICommand command, params ToolStripItem[] items)
        {
            foreach (ToolStripItem item in items)
            {
                // Bind visual display and the user interaction to the command.
                item.Text = command.Name;
                item.Image = command.Image;
                item.Click += (s, e) => { if (command.Execute()) UpdateCommands(); };
                switch (item)
                {
                    case ToolStripMenuItem menuItem:
                        menuItem.ShortcutKeys = command.ShortcutKeys;
                        break;
                }
            }
            _commandBindings.Add(command, items);
        }
        
        private void UpdateCommands()
        {
            foreach (KeyValuePair<ICommand, ToolStripItem[]> commandBinding in _commandBindings)
            {
                // Only update enabled and checked state for now.
                bool canExecute = commandBinding.Key.CanExecute;
                bool isChecked = commandBinding.Key.IsChecked;
                foreach (ToolStripItem item in commandBinding.Value)
                {
                    item.Enabled = canExecute;
                    switch (item)
                    {
                        case ToolStripMenuItem menuItem:
                            menuItem.Checked = isChecked;
                            break;
                        case ToolStripButton buttonItem:
                            buttonItem.Checked = isChecked;
                            break;
                    }
                }
            }
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void _controller_LevelChanged(object sender, EventArgs e)
        {
            if (_controller.Level == null)
            {
                Text = AssemblyInfo.Title;
            }
            else
            {
                if (_controller.LevelFileName != null)
                {
                    Text = $"{AssemblyInfo.Title} - {Path.GetFileName(_controller.LevelFileName)}";
                }
                else
                {
                    Text = $"{AssemblyInfo.Title} - New Level";
                }

                // Update tile pickers and map view.
                _actorPicker.ActorBitmap = _controller.GameData.ActorBitmap;
                _levelView.SetGameDataAndLevel(_controller.GameData, _controller.Level);
                _levelView.Refresh();
            }
        }

        private void _controller_LevelWorldChanged(object sender, EventArgs e)
        {
            int worldIndex = (int)_controller.Level.World - 1;
            
            _backPicker.TileImage = _controller.GameData.BackImages[worldIndex];
            _forePicker.TileImage = _controller.GameData.ForeImages[worldIndex]; 
            _levelView.SetGameDataAndLevel(_controller.GameData, _controller.Level);
        }
        
        private void _controller_ActiveLayerChanged(object sender, EventArgs e)
        {
            _levelView.ActiveLayer = _controller.ActiveLayer;
        }

        private void _controller_FadeInactiveLayersChanged(object sender, EventArgs e)
        {
            _levelView.FadeInactiveLayers = _controller.FadeInactiveLayers;
        }

        private void _controller_ParallaxVisibleChanged(object sender, EventArgs e)
        {
            _levelView.ParallaxVisible = _controller.ParallaxVisible;
        }
    }
}
