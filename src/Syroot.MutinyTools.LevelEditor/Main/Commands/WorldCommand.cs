﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Syroot.MutinyTools.Level;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class WorldCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;
        private LevelWorld _world;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public WorldCommand(MainController controller, LevelWorld world)
        {
            _controller = controller;
            _world = world;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get
            {
                switch (_world)
                {
                    case LevelWorld.Industrial: return Resources.World_Industrial;
                    case LevelWorld.Wilderness: return Resources.World_Wilderness;
                    case LevelWorld.Ice: return Resources.World_Ice;
                    case LevelWorld.Space: return Resources.World_Space;
                    default: throw new ArgumentException("Invalid level world in command.", nameof(_world));
                }
            }
        }

        public string Name
        {
            get { return "&" + _world.ToString(); }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.Control | (Keys.D0 + (int)_world); }
        }

        public bool CanExecute
        {
            get { return _controller.Level != null; }
        }

        public bool IsChecked
        {
            get { return _controller.Level?.World == _world; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            _controller.SetLevelWorld(_world);
            return true;
        }
    }
}