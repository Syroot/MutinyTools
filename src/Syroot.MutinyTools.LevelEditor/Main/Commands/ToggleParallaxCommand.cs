﻿using System.Drawing;
using System.Windows.Forms;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class ToggleParallaxCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public ToggleParallaxCommand(MainController controller)
        {
            _controller = controller;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.SendtoBack_16x; }
        }

        public string Name
        {
            get { return "Show &Parallax"; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.None; }
        }

        public bool CanExecute
        {
            get { return true; }
        }

        public bool IsChecked
        {
            get { return _controller.ParallaxVisible; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            _controller.ParallaxVisible = !_controller.ParallaxVisible;
            return true;
        }
    }
}