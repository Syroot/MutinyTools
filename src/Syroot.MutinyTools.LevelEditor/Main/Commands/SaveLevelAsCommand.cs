﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class SaveLevelAsCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;
        private SaveFileDialog _sfd;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public SaveLevelAsCommand(MainController controller)
        {
            _controller = controller;
            _sfd = new SaveFileDialog()
            {
                Title = "Save Level",
                Filter = "MOTT level files|*.lvl"
            };
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.SaveFileDialogControl_16x; }
        }

        public string Name
        {
            get { return "Save Level &As..."; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.Control | Keys.Alt | Keys.S; }
        }

        public bool CanExecute
        {
            get { return _controller.Level != null; }
        }

        public bool IsChecked
        {
            get { return false; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            _sfd.InitialDirectory = Path.GetDirectoryName(_controller.LevelFileName);
            _sfd.FileName = Path.GetFileName(_controller.LevelFileName);
            if (_sfd.ShowDialog() == DialogResult.OK)
            {
                _controller.LevelFileName = _sfd.FileName;
                _controller.Level.Save(_controller.LevelFileName);
                return true;
            }
            return false;
        }
    }
}