﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Syroot.MutinyTools.Container;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class PackContainerCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;
        private FolderBrowserDialog _fbd;
        private SaveFileDialog _sfd;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public PackContainerCommand(MainController controller)
        {
            _controller = controller;
            _fbd = new FolderBrowserDialog()
            {
                Description = "Select the folder which contains the game data to pack.",
                ShowNewFolderButton = false
            };
            _sfd = new SaveFileDialog()
            {
                Title = "Save Container",
                Filter = "MOTT data containers|*.cnt"
            };
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.CreatePackage_16x; }
        }

        public string Name
        {
            get { return "&Pack Container..."; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.None; }
        }

        public bool CanExecute
        {
            get { return true; }
        }

        public bool IsChecked
        {
            get { return false; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            if (_fbd.ShowDialog() == DialogResult.OK)
            {
                // Suggest storing a container with the same name as the directory in the parent folder.
                _sfd.InitialDirectory = Path.GetDirectoryName(_fbd.SelectedPath);
                _sfd.FileName = Path.ChangeExtension(Path.GetFileName(_fbd.SelectedPath), "cnt");
                if (_sfd.ShowDialog() == DialogResult.OK)
                {
                    // Put all files found in the directory into the container.
                    using (ContainerFile container = new ContainerFile())
                    {
                        string[] files = Directory.GetFiles(_fbd.SelectedPath);
                        foreach (string file in files)
                        {
                            string fileName = Path.GetFileName(file);
                            container.Entries.Add(fileName, new FileContainerEntry(file));
                        }
                        // Save the container.
                        container.Save(_sfd.FileName);
                        MessageBox.Show($"Packed {files.Length} files into \"{_sfd.FileName}\".",
                            AssemblyInfo.Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            return false;
        }
    }
}