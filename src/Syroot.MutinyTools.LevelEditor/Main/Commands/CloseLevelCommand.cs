﻿using System.Drawing;
using System.Windows.Forms;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class CloseLevelCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public CloseLevelCommand(MainController controller)
        {
            _controller = controller;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.CloseDocument_16x; }
        }

        public string Name
        {
            get { return "&Close Level"; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.Control | Keys.W; }
        }

        public bool CanExecute
        {
            get { return _controller.Level != null; }
        }

        public bool IsChecked
        {
            get { return false; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            _controller.Level = null;
            _controller.LevelFileName = null;
            return true;
        }
    }
}