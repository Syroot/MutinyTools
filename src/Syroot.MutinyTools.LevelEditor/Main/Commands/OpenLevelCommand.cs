﻿using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Syroot.MutinyTools.Level;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class OpenLevelCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;
        private OpenFileDialog _ofd;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public OpenLevelCommand(MainController controller)
        {
            _controller = controller;
            _ofd = new OpenFileDialog()
            {
                Title = "Open Level",
                Filter = "MOTT level files|*.lvl"
            };
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.OpenFolder_16x; }
        }

        public string Name
        {
            get { return "&Open Level..."; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.Control | Keys.O; }
        }

        public bool CanExecute
        {
            get { return true; }
        }

        public bool IsChecked
        {
            get { return false; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            _ofd.InitialDirectory = Path.GetDirectoryName(_controller.LevelFileName);
            _ofd.FileName = Path.GetFileName(_controller.LevelFileName);
            if (_ofd.ShowDialog() == DialogResult.OK)
            {
                _controller.LevelFileName = _ofd.FileName;
                _controller.GameData.DirectoryPath = Path.GetDirectoryName(_controller.LevelFileName);
                _controller.Level = new LevelFile(_controller.LevelFileName);
                return true;
            }
            return false;
        }
    }
}