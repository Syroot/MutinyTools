﻿using System.Drawing;
using System.Windows.Forms;
using Syroot.MutinyTools.Level;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class NewLevelCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;
        private FolderBrowserDialog _fbd;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public NewLevelCommand(MainController controller)
        {
            _controller = controller;
            _fbd = new FolderBrowserDialog()
            {
#if DEBUG
                SelectedPath = @"D:\Archive\Games\Emulators\DOSBox\_DOS Harddisk\DADD\DADD",
#endif
                Description = "Please select the folder in which game data is stored."
            };
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.NewFile_16x; }
        }

        public string Name
        {
            get { return "&New Level..."; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.Control | Keys.N; }
        }

        public bool CanExecute
        {
            get { return true; }
        }

        public bool IsChecked
        {
            get { return false; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            if (_fbd.ShowDialog() == DialogResult.OK)
            {
                _controller.GameData.DirectoryPath = _fbd.SelectedPath;
                _controller.Level = new LevelFile();
                return true;
            }
            return false;
        }
    }
}