﻿using System.Drawing;
using System.Windows.Forms;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class SaveLevelCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public SaveLevelCommand(MainController controller)
        {
            _controller = controller;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.Save_16x; }
        }

        public string Name
        {
            get { return "&Save Level"; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.Control | Keys.S; }
        }

        public bool CanExecute
        {
            get { return _controller.LevelFileName != null; }
        }

        public bool IsChecked
        {
            get { return false; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            _controller.Level.Save(_controller.LevelFileName);
            return false;
        }
    }
}