﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Syroot.MutinyTools.Container;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class UnpackContainerCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;
        private OpenFileDialog _ofd;
        private FolderBrowserDialog _fbd;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public UnpackContainerCommand(MainController controller)
        {
            _controller = controller;
            _ofd = new OpenFileDialog()
            {
                Title = "Open Container",
                Filter = "MOTT data containers|*.cnt"
            };
            _fbd = new FolderBrowserDialog()
            {
                Description = "Please select the folder in which you want to unpack game data to."
            };
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.PackageFolderOpen_16x; }
        }

        public string Name
        {
            get { return "&Unpack Container..."; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.None; }
        }

        public bool CanExecute
        {
            get { return true; }
        }

        public bool IsChecked
        {
            get { return false; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            if (_ofd.ShowDialog() == DialogResult.OK)
            {
                // Suggest unpacking into the parent directory.
                _fbd.SelectedPath = Path.GetDirectoryName(_ofd.FileName);
                if (_fbd.ShowDialog() == DialogResult.OK)
                {
                    // Unpack all files into the directory.
                    using (ContainerFile container = new ContainerFile(_ofd.FileName))
                    {
                        foreach (KeyValuePair<string, IContainerEntry> entry in container.Entries)
                        {
                            string outputFile = Path.Combine(_fbd.SelectedPath, entry.Key);
                            using (FileStream stream
                                = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
                            {
                                entry.Value.CopyDataTo(stream);
                            }
                        }
                        MessageBox.Show($"Unpacked {container.Entries.Count} files into \"{_fbd.SelectedPath}\".",
                            AssemblyInfo.Title, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            return false;
        }
    }
}