﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class LayerCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;
        private LevelLayer _layer;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public LayerCommand(MainController controller, LevelLayer layer)
        {
            _controller = controller;
            _layer = layer;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get
            {
                switch (_layer)
                {
                    case LevelLayer.Background: return Resources.BackgroundColor_16x;
                    case LevelLayer.Items: return Resources.GridUniform_16x;
                    case LevelLayer.Actors: return Resources.Actor_16x;
                    case LevelLayer.Foreground: return Resources.ForegroundColor_16x;
                    default: throw new ArgumentException("Invalid layer in command.", nameof(_layer));
                }
            }
        }

        public string Name
        {
            get { return "&" + _layer.ToString(); }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.Alt | (Keys.D1 + (int)_layer); }
        }

        public bool CanExecute
        {
            get { return true; }
        }

        public bool IsChecked
        {
            get { return _controller.ActiveLayer == _layer; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            _controller.ActiveLayer = _layer;
            return true;
        }
    }
}