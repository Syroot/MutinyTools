﻿using System.Drawing;
using System.Windows.Forms;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class ToggleFadeCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public ToggleFadeCommand(MainController controller)
        {
            _controller = controller;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.TransparentBackground_16x; }
        }

        public string Name
        {
            get { return "&Fade Inactive Layers"; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.None; }
        }

        public bool CanExecute
        {
            get { return true; }
        }

        public bool IsChecked
        {
            get { return _controller.FadeInactiveLayers; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            _controller.FadeInactiveLayers = !_controller.FadeInactiveLayers;
            return true;
        }
    }
}