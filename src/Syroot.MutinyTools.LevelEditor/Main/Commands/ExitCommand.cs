﻿using System.Drawing;
using System.Windows.Forms;
using Syroot.MutinyTools.LevelEditor.Properties;

namespace Syroot.MutinyTools.LevelEditor.Main.Commands
{
    internal class ExitCommand : ICommand
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MainController _controller;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public ExitCommand(MainController controller)
        {
            _controller = controller;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Image Image
        {
            get { return Resources.Close_16xLG; }
        }

        public string Name
        {
            get { return "E&xit"; }
        }

        public Keys ShortcutKeys
        {
            get { return Keys.Alt | Keys.F4; }
        }

        public bool CanExecute
        {
            get { return true; }
        }

        public bool IsChecked
        {
            get { return false; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public bool Execute()
        {
            _controller.ExitApplication();
            return false;
        }
    }
}