﻿namespace Syroot.MutinyTools.LevelEditor.Main
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this._msMain = new System.Windows.Forms.MenuStrip();
            this._tsmiMainFile = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainFileClose = new System.Windows.Forms.ToolStripMenuItem();
            this._tssMainFile1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsmiMainFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainWorld = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainWorldIndustrial = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainWorldWilderness = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainWorldIce = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainWorldSpace = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainLayer = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainLayerBackground = new System.Windows.Forms.ToolStripMenuItem();
            this._tssMainLayer1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsmiMainLayerTiles = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainLayerActors = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainLayerForeground = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainView = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainViewFadeInactive = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainViewParallax = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainTools = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainToolsPack = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmiMainToolsUnpack = new System.Windows.Forms.ToolStripMenuItem();
            this._forePicker = new Syroot.MutinyTools.LevelEditor.UI.TilePickerGridView();
            this._actorPicker = new Syroot.MutinyTools.LevelEditor.UI.ActorPickerGridView();
            this._levelView = new Syroot.MutinyTools.LevelEditor.UI.LevelGridView();
            this._backPicker = new Syroot.MutinyTools.LevelEditor.UI.TilePickerGridView();
            this._tsMain = new System.Windows.Forms.ToolStrip();
            this._tsbMainFileNew = new System.Windows.Forms.ToolStripButton();
            this._tsbMainFileOpen = new System.Windows.Forms.ToolStripButton();
            this._tsbMainFileSave = new System.Windows.Forms.ToolStripButton();
            this._tsbMainFileSaveAs = new System.Windows.Forms.ToolStripButton();
            this._tsbMainFileClose = new System.Windows.Forms.ToolStripButton();
            this._tssMain1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsbMainWorldIndustrial = new System.Windows.Forms.ToolStripButton();
            this._tsbMainWorldWilderness = new System.Windows.Forms.ToolStripButton();
            this._tsbMainWorldIce = new System.Windows.Forms.ToolStripButton();
            this._tsbMainWorldSpace = new System.Windows.Forms.ToolStripButton();
            this._tssMain2 = new System.Windows.Forms.ToolStripSeparator();
            this._tsbMainLayerBackground = new System.Windows.Forms.ToolStripButton();
            this._tsbMainLayerTiles = new System.Windows.Forms.ToolStripButton();
            this._tsbMainLayerActors = new System.Windows.Forms.ToolStripButton();
            this._tsbMainLayerForeground = new System.Windows.Forms.ToolStripButton();
            this._tssMain3 = new System.Windows.Forms.ToolStripSeparator();
            this._tsbMainViewFadeInactive = new System.Windows.Forms.ToolStripButton();
            this._tsbMainViewParallax = new System.Windows.Forms.ToolStripButton();
            this._tssMain4 = new System.Windows.Forms.ToolStripSeparator();
            this._tsbMainToolsPack = new System.Windows.Forms.ToolStripButton();
            this._tsbMainToolsUnpack = new System.Windows.Forms.ToolStripButton();
            this._msMain.SuspendLayout();
            this._tsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // _msMain
            // 
            this._msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiMainFile,
            this._tsmiMainWorld,
            this._tsmiMainLayer,
            this._tsmiMainView,
            this._tsmiMainTools});
            this._msMain.Location = new System.Drawing.Point(0, 0);
            this._msMain.Name = "_msMain";
            this._msMain.Padding = new System.Windows.Forms.Padding(2);
            this._msMain.Size = new System.Drawing.Size(984, 24);
            this._msMain.TabIndex = 0;
            this._msMain.Text = "Main";
            // 
            // _tsmiMainFile
            // 
            this._tsmiMainFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiMainFileNew,
            this._tsmiMainFileOpen,
            this._tsmiMainFileSave,
            this._tsmiMainFileSaveAs,
            this._tsmiMainFileClose,
            this._tssMainFile1,
            this._tsmiMainFileExit});
            this._tsmiMainFile.Name = "_tsmiMainFile";
            this._tsmiMainFile.Size = new System.Drawing.Size(37, 20);
            this._tsmiMainFile.Text = "File";
            // 
            // _tsmiMainFileNew
            // 
            this._tsmiMainFileNew.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.NewFile_16x;
            this._tsmiMainFileNew.Name = "_tsmiMainFileNew";
            this._tsmiMainFileNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._tsmiMainFileNew.Size = new System.Drawing.Size(216, 22);
            this._tsmiMainFileNew.Text = "New Level...";
            // 
            // _tsmiMainFileOpen
            // 
            this._tsmiMainFileOpen.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.OpenFolder_16x;
            this._tsmiMainFileOpen.Name = "_tsmiMainFileOpen";
            this._tsmiMainFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._tsmiMainFileOpen.Size = new System.Drawing.Size(216, 22);
            this._tsmiMainFileOpen.Text = "&Open Level...";
            // 
            // _tsmiMainFileSave
            // 
            this._tsmiMainFileSave.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.Save_16x;
            this._tsmiMainFileSave.Name = "_tsmiMainFileSave";
            this._tsmiMainFileSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._tsmiMainFileSave.Size = new System.Drawing.Size(216, 22);
            this._tsmiMainFileSave.Text = "&Save Level";
            // 
            // _tsmiMainFileSaveAs
            // 
            this._tsmiMainFileSaveAs.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.SaveFileDialogControl_16x;
            this._tsmiMainFileSaveAs.Name = "_tsmiMainFileSaveAs";
            this._tsmiMainFileSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this._tsmiMainFileSaveAs.Size = new System.Drawing.Size(216, 22);
            this._tsmiMainFileSaveAs.Text = "Save Level &As...";
            // 
            // _tsmiMainFileClose
            // 
            this._tsmiMainFileClose.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.CloseDocument_16x;
            this._tsmiMainFileClose.Name = "_tsmiMainFileClose";
            this._tsmiMainFileClose.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this._tsmiMainFileClose.Size = new System.Drawing.Size(216, 22);
            this._tsmiMainFileClose.Text = "Close Level";
            // 
            // _tssMainFile1
            // 
            this._tssMainFile1.Name = "_tssMainFile1";
            this._tssMainFile1.Size = new System.Drawing.Size(213, 6);
            // 
            // _tsmiMainFileExit
            // 
            this._tsmiMainFileExit.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.Close_16xLG;
            this._tsmiMainFileExit.Name = "_tsmiMainFileExit";
            this._tsmiMainFileExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this._tsmiMainFileExit.Size = new System.Drawing.Size(216, 22);
            this._tsmiMainFileExit.Text = "E&xit";
            // 
            // _tsmiMainWorld
            // 
            this._tsmiMainWorld.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiMainWorldIndustrial,
            this._tsmiMainWorldWilderness,
            this._tsmiMainWorldIce,
            this._tsmiMainWorldSpace});
            this._tsmiMainWorld.Name = "_tsmiMainWorld";
            this._tsmiMainWorld.Size = new System.Drawing.Size(51, 20);
            this._tsmiMainWorld.Text = "World";
            // 
            // _tsmiMainWorldIndustrial
            // 
            this._tsmiMainWorldIndustrial.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.World_Industrial;
            this._tsmiMainWorldIndustrial.Name = "_tsmiMainWorldIndustrial";
            this._tsmiMainWorldIndustrial.Size = new System.Drawing.Size(131, 22);
            this._tsmiMainWorldIndustrial.Text = "Industrial";
            // 
            // _tsmiMainWorldWilderness
            // 
            this._tsmiMainWorldWilderness.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.World_Wilderness;
            this._tsmiMainWorldWilderness.Name = "_tsmiMainWorldWilderness";
            this._tsmiMainWorldWilderness.Size = new System.Drawing.Size(131, 22);
            this._tsmiMainWorldWilderness.Text = "Wilderness";
            // 
            // _tsmiMainWorldIce
            // 
            this._tsmiMainWorldIce.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.World_Ice;
            this._tsmiMainWorldIce.Name = "_tsmiMainWorldIce";
            this._tsmiMainWorldIce.Size = new System.Drawing.Size(131, 22);
            this._tsmiMainWorldIce.Text = "Ice";
            // 
            // _tsmiMainWorldSpace
            // 
            this._tsmiMainWorldSpace.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.World_Space;
            this._tsmiMainWorldSpace.Name = "_tsmiMainWorldSpace";
            this._tsmiMainWorldSpace.Size = new System.Drawing.Size(131, 22);
            this._tsmiMainWorldSpace.Text = "Space";
            // 
            // _tsmiMainLayer
            // 
            this._tsmiMainLayer.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiMainLayerBackground,
            this._tssMainLayer1,
            this._tsmiMainLayerTiles,
            this._tsmiMainLayerActors,
            this._tsmiMainLayerForeground});
            this._tsmiMainLayer.Name = "_tsmiMainLayer";
            this._tsmiMainLayer.Size = new System.Drawing.Size(47, 20);
            this._tsmiMainLayer.Text = "Layer";
            // 
            // _tsmiMainLayerBackground
            // 
            this._tsmiMainLayerBackground.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.BackgroundColor_16x;
            this._tsmiMainLayerBackground.Name = "_tsmiMainLayerBackground";
            this._tsmiMainLayerBackground.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D1)));
            this._tsmiMainLayerBackground.Size = new System.Drawing.Size(174, 22);
            this._tsmiMainLayerBackground.Text = "Background";
            // 
            // _tssMainLayer1
            // 
            this._tssMainLayer1.Name = "_tssMainLayer1";
            this._tssMainLayer1.Size = new System.Drawing.Size(171, 6);
            // 
            // _tsmiMainLayerTiles
            // 
            this._tsmiMainLayerTiles.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.GridUniform_16x;
            this._tsmiMainLayerTiles.Name = "_tsmiMainLayerTiles";
            this._tsmiMainLayerTiles.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D2)));
            this._tsmiMainLayerTiles.Size = new System.Drawing.Size(174, 22);
            this._tsmiMainLayerTiles.Text = "Tiles";
            // 
            // _tsmiMainLayerActors
            // 
            this._tsmiMainLayerActors.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.Actor_16x;
            this._tsmiMainLayerActors.Name = "_tsmiMainLayerActors";
            this._tsmiMainLayerActors.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D3)));
            this._tsmiMainLayerActors.Size = new System.Drawing.Size(174, 22);
            this._tsmiMainLayerActors.Text = "Actors";
            // 
            // _tsmiMainLayerForeground
            // 
            this._tsmiMainLayerForeground.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.ForegroundColor_16x;
            this._tsmiMainLayerForeground.Name = "_tsmiMainLayerForeground";
            this._tsmiMainLayerForeground.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D4)));
            this._tsmiMainLayerForeground.Size = new System.Drawing.Size(174, 22);
            this._tsmiMainLayerForeground.Text = "Foreground";
            // 
            // _tsmiMainView
            // 
            this._tsmiMainView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiMainViewFadeInactive,
            this._tsmiMainViewParallax});
            this._tsmiMainView.Name = "_tsmiMainView";
            this._tsmiMainView.Size = new System.Drawing.Size(44, 20);
            this._tsmiMainView.Text = "View";
            // 
            // _tsmiMainViewFadeInactive
            // 
            this._tsmiMainViewFadeInactive.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.TransparentBackground_16x;
            this._tsmiMainViewFadeInactive.Name = "_tsmiMainViewFadeInactive";
            this._tsmiMainViewFadeInactive.Size = new System.Drawing.Size(179, 22);
            this._tsmiMainViewFadeInactive.Text = "Fade Inactive Layers";
            // 
            // _tsmiMainViewParallax
            // 
            this._tsmiMainViewParallax.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.SendtoBack_16x;
            this._tsmiMainViewParallax.Name = "_tsmiMainViewParallax";
            this._tsmiMainViewParallax.Size = new System.Drawing.Size(179, 22);
            this._tsmiMainViewParallax.Text = "Show Parallax";
            // 
            // _tsmiMainTools
            // 
            this._tsmiMainTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmiMainToolsPack,
            this._tsmiMainToolsUnpack});
            this._tsmiMainTools.Name = "_tsmiMainTools";
            this._tsmiMainTools.Size = new System.Drawing.Size(47, 20);
            this._tsmiMainTools.Text = "Tools";
            // 
            // _tsmiMainToolsPack
            // 
            this._tsmiMainToolsPack.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.CreatePackage_16x;
            this._tsmiMainToolsPack.Name = "_tsmiMainToolsPack";
            this._tsmiMainToolsPack.Size = new System.Drawing.Size(178, 22);
            this._tsmiMainToolsPack.Text = "Pack Container...";
            // 
            // _tsmiMainToolsUnpack
            // 
            this._tsmiMainToolsUnpack.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.PackageFolderOpen_16x;
            this._tsmiMainToolsUnpack.Name = "_tsmiMainToolsUnpack";
            this._tsmiMainToolsUnpack.Size = new System.Drawing.Size(178, 22);
            this._tsmiMainToolsUnpack.Text = "Unpack Container...";
            // 
            // _foregroundPicker
            // 
            this._forePicker.BackColor = System.Drawing.Color.Black;
            this._forePicker.Location = new System.Drawing.Point(0, 245);
            this._forePicker.Margin = new System.Windows.Forms.Padding(2);
            this._forePicker.Name = "_foregroundPicker";
            this._forePicker.Size = new System.Drawing.Size(320, 192);
            this._forePicker.TabIndex = 1;
            this._forePicker.Zoomable = false;
            this._forePicker.ZoomStep = 0F;
            // 
            // _actorPicker
            // 
            this._actorPicker.BackColor = System.Drawing.Color.Black;
            this._actorPicker.Location = new System.Drawing.Point(0, 441);
            this._actorPicker.Margin = new System.Windows.Forms.Padding(2);
            this._actorPicker.Name = "_actorPicker";
            this._actorPicker.Scrollable = false;
            this._actorPicker.Size = new System.Drawing.Size(320, 100);
            this._actorPicker.TabIndex = 3;
            this._actorPicker.Zoomable = false;
            this._actorPicker.ZoomStep = 0F;
            // 
            // _levelView
            // 
            this._levelView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._levelView.BackColor = System.Drawing.Color.Black;
            this._levelView.Location = new System.Drawing.Point(324, 49);
            this._levelView.Margin = new System.Windows.Forms.Padding(2);
            this._levelView.Name = "_levelView";
            this._levelView.Size = new System.Drawing.Size(660, 512);
            this._levelView.TabIndex = 4;
            this._levelView.Zoom = 2F;
            this._levelView.ZoomMax = 8F;
            this._levelView.ZoomMin = 0.25F;
            this._levelView.ZoomStep = 0.5F;
            // 
            // _backgroundPicker
            // 
            this._backPicker.BackColor = System.Drawing.Color.Black;
            this._backPicker.Location = new System.Drawing.Point(0, 49);
            this._backPicker.Margin = new System.Windows.Forms.Padding(2);
            this._backPicker.Name = "_backgroundPicker";
            this._backPicker.Size = new System.Drawing.Size(320, 192);
            this._backPicker.TabIndex = 2;
            this._backPicker.Zoomable = false;
            this._backPicker.ZoomStep = 0F;
            // 
            // _tsMain
            // 
            this._tsMain.AutoSize = false;
            this._tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsbMainFileNew,
            this._tsbMainFileOpen,
            this._tsbMainFileSave,
            this._tsbMainFileSaveAs,
            this._tsbMainFileClose,
            this._tssMain1,
            this._tsbMainWorldIndustrial,
            this._tsbMainWorldWilderness,
            this._tsbMainWorldIce,
            this._tsbMainWorldSpace,
            this._tssMain2,
            this._tsbMainLayerBackground,
            this._tsbMainLayerTiles,
            this._tsbMainLayerActors,
            this._tsbMainLayerForeground,
            this._tssMain3,
            this._tsbMainViewFadeInactive,
            this._tsbMainViewParallax,
            this._tssMain4,
            this._tsbMainToolsPack,
            this._tsbMainToolsUnpack});
            this._tsMain.Location = new System.Drawing.Point(0, 24);
            this._tsMain.Name = "_tsMain";
            this._tsMain.Padding = new System.Windows.Forms.Padding(2, 1, 2, 0);
            this._tsMain.Size = new System.Drawing.Size(984, 27);
            this._tsMain.TabIndex = 5;
            this._tsMain.Text = "Main";
            // 
            // _tsbMainFileNew
            // 
            this._tsbMainFileNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainFileNew.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.NewFile_16x;
            this._tsbMainFileNew.Name = "_tsbMainFileNew";
            this._tsbMainFileNew.Size = new System.Drawing.Size(23, 23);
            this._tsbMainFileNew.Text = "New Level...";
            // 
            // _tsbMainFileOpen
            // 
            this._tsbMainFileOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainFileOpen.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.OpenFolder_16x;
            this._tsbMainFileOpen.Name = "_tsbMainFileOpen";
            this._tsbMainFileOpen.Size = new System.Drawing.Size(23, 23);
            this._tsbMainFileOpen.Text = "Open Level...";
            // 
            // _tsbMainFileSave
            // 
            this._tsbMainFileSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainFileSave.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.Save_16x;
            this._tsbMainFileSave.Name = "_tsbMainFileSave";
            this._tsbMainFileSave.Size = new System.Drawing.Size(23, 23);
            this._tsbMainFileSave.Text = "Save Level";
            // 
            // _tsbMainFileSaveAs
            // 
            this._tsbMainFileSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainFileSaveAs.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.SaveFileDialogControl_16x;
            this._tsbMainFileSaveAs.Name = "_tsbMainFileSaveAs";
            this._tsbMainFileSaveAs.Size = new System.Drawing.Size(23, 23);
            this._tsbMainFileSaveAs.Text = "Save Level As...";
            // 
            // _tsbMainFileClose
            // 
            this._tsbMainFileClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainFileClose.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.CloseDocument_16x;
            this._tsbMainFileClose.Name = "_tsbMainFileClose";
            this._tsbMainFileClose.Size = new System.Drawing.Size(23, 23);
            this._tsbMainFileClose.Text = "Close Level";
            // 
            // _tssMain1
            // 
            this._tssMain1.Name = "_tssMain1";
            this._tssMain1.Size = new System.Drawing.Size(6, 26);
            // 
            // _tsbMainWorldIndustrial
            // 
            this._tsbMainWorldIndustrial.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainWorldIndustrial.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.World_Industrial;
            this._tsbMainWorldIndustrial.Name = "_tsbMainWorldIndustrial";
            this._tsbMainWorldIndustrial.Size = new System.Drawing.Size(23, 23);
            this._tsbMainWorldIndustrial.Text = "Industrial";
            // 
            // _tsbMainWorldWilderness
            // 
            this._tsbMainWorldWilderness.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainWorldWilderness.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.World_Wilderness;
            this._tsbMainWorldWilderness.Name = "_tsbMainWorldWilderness";
            this._tsbMainWorldWilderness.Size = new System.Drawing.Size(23, 23);
            this._tsbMainWorldWilderness.Text = "Wilderness";
            // 
            // _tsbMainWorldIce
            // 
            this._tsbMainWorldIce.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainWorldIce.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.World_Ice;
            this._tsbMainWorldIce.Name = "_tsbMainWorldIce";
            this._tsbMainWorldIce.Size = new System.Drawing.Size(23, 23);
            this._tsbMainWorldIce.Text = "Ice";
            // 
            // _tsbMainWorldSpace
            // 
            this._tsbMainWorldSpace.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainWorldSpace.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.World_Space;
            this._tsbMainWorldSpace.Name = "_tsbMainWorldSpace";
            this._tsbMainWorldSpace.Size = new System.Drawing.Size(23, 23);
            this._tsbMainWorldSpace.Text = "Space";
            // 
            // _tssMain2
            // 
            this._tssMain2.Name = "_tssMain2";
            this._tssMain2.Size = new System.Drawing.Size(6, 26);
            // 
            // _tsbMainLayerBackground
            // 
            this._tsbMainLayerBackground.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainLayerBackground.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.BackgroundColor_16x;
            this._tsbMainLayerBackground.Name = "_tsbMainLayerBackground";
            this._tsbMainLayerBackground.Size = new System.Drawing.Size(23, 23);
            this._tsbMainLayerBackground.Text = "Background";
            // 
            // _tsbMainLayerTiles
            // 
            this._tsbMainLayerTiles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainLayerTiles.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.GridUniform_16x;
            this._tsbMainLayerTiles.Name = "_tsbMainLayerTiles";
            this._tsbMainLayerTiles.Size = new System.Drawing.Size(23, 23);
            this._tsbMainLayerTiles.Text = "Tiles";
            // 
            // _tsbMainLayerActors
            // 
            this._tsbMainLayerActors.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainLayerActors.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.Actor_16x;
            this._tsbMainLayerActors.Name = "_tsbMainLayerActors";
            this._tsbMainLayerActors.Size = new System.Drawing.Size(23, 23);
            this._tsbMainLayerActors.Text = "Actors";
            // 
            // _tsbMainLayerForeground
            // 
            this._tsbMainLayerForeground.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainLayerForeground.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.ForegroundColor_16x;
            this._tsbMainLayerForeground.Name = "_tsbMainLayerForeground";
            this._tsbMainLayerForeground.Size = new System.Drawing.Size(23, 23);
            this._tsbMainLayerForeground.Text = "Foreground";
            // 
            // _tssMain3
            // 
            this._tssMain3.Name = "_tssMain3";
            this._tssMain3.Size = new System.Drawing.Size(6, 26);
            // 
            // _tsbMainViewFadeInactive
            // 
            this._tsbMainViewFadeInactive.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainViewFadeInactive.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.TransparentBackground_16x;
            this._tsbMainViewFadeInactive.Name = "_tsbMainViewFadeInactive";
            this._tsbMainViewFadeInactive.Size = new System.Drawing.Size(23, 23);
            this._tsbMainViewFadeInactive.Text = "Fade Inactive Layers";
            // 
            // _tsbMainViewParallax
            // 
            this._tsbMainViewParallax.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainViewParallax.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.SendtoBack_16x;
            this._tsbMainViewParallax.Name = "_tsbMainViewParallax";
            this._tsbMainViewParallax.Size = new System.Drawing.Size(23, 23);
            this._tsbMainViewParallax.Text = "Show Parallax";
            // 
            // _tssMain4
            // 
            this._tssMain4.Name = "_tssMain4";
            this._tssMain4.Size = new System.Drawing.Size(6, 26);
            // 
            // _tsbMainToolsPack
            // 
            this._tsbMainToolsPack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainToolsPack.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.CreatePackage_16x;
            this._tsbMainToolsPack.Name = "_tsbMainToolsPack";
            this._tsbMainToolsPack.Size = new System.Drawing.Size(23, 23);
            this._tsbMainToolsPack.Text = "Pack Container...";
            // 
            // _tsbMainToolsUnpack
            // 
            this._tsbMainToolsUnpack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsbMainToolsUnpack.Image = global::Syroot.MutinyTools.LevelEditor.Properties.Resources.PackageFolderOpen_16x;
            this._tsbMainToolsUnpack.Name = "_tsbMainToolsUnpack";
            this._tsbMainToolsUnpack.Size = new System.Drawing.Size(23, 23);
            this._tsbMainToolsUnpack.Text = "Unpack Container...";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this._tsMain);
            this.Controls.Add(this._msMain);
            this.Controls.Add(this._forePicker);
            this.Controls.Add(this._backPicker);
            this.Controls.Add(this._levelView);
            this.Controls.Add(this._actorPicker);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._msMain;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MOTT Level Editor";
            this._msMain.ResumeLayout(false);
            this._msMain.PerformLayout();
            this._tsMain.ResumeLayout(false);
            this._tsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _msMain;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainFile;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainFileOpen;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainFileSave;
        private System.Windows.Forms.ToolStripSeparator _tssMainFile1;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainFileExit;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainWorld;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainWorldIndustrial;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainWorldWilderness;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainWorldIce;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainWorldSpace;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainLayer;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainLayerBackground;
        private System.Windows.Forms.ToolStripSeparator _tssMainLayer1;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainLayerTiles;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainLayerForeground;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainLayerActors;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainFileNew;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainFileSaveAs;
        private UI.TilePickerGridView _forePicker;
        private UI.ActorPickerGridView _actorPicker;
        private UI.LevelGridView _levelView;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainFileClose;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainTools;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainToolsPack;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainToolsUnpack;
        private UI.TilePickerGridView _backPicker;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainView;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainViewParallax;
        private System.Windows.Forms.ToolStripMenuItem _tsmiMainViewFadeInactive;
        private System.Windows.Forms.ToolStrip _tsMain;
        private System.Windows.Forms.ToolStripButton _tsbMainFileNew;
        private System.Windows.Forms.ToolStripButton _tsbMainFileOpen;
        private System.Windows.Forms.ToolStripButton _tsbMainFileSave;
        private System.Windows.Forms.ToolStripButton _tsbMainFileSaveAs;
        private System.Windows.Forms.ToolStripButton _tsbMainFileClose;
        private System.Windows.Forms.ToolStripSeparator _tssMain1;
        private System.Windows.Forms.ToolStripButton _tsbMainWorldIndustrial;
        private System.Windows.Forms.ToolStripButton _tsbMainWorldWilderness;
        private System.Windows.Forms.ToolStripButton _tsbMainWorldIce;
        private System.Windows.Forms.ToolStripButton _tsbMainWorldSpace;
        private System.Windows.Forms.ToolStripSeparator _tssMain2;
        private System.Windows.Forms.ToolStripButton _tsbMainLayerBackground;
        private System.Windows.Forms.ToolStripButton _tsbMainLayerTiles;
        private System.Windows.Forms.ToolStripButton _tsbMainLayerActors;
        private System.Windows.Forms.ToolStripButton _tsbMainLayerForeground;
        private System.Windows.Forms.ToolStripSeparator _tssMain3;
        private System.Windows.Forms.ToolStripButton _tsbMainViewFadeInactive;
        private System.Windows.Forms.ToolStripButton _tsbMainViewParallax;
        private System.Windows.Forms.ToolStripSeparator _tssMain4;
        private System.Windows.Forms.ToolStripButton _tsbMainToolsPack;
        private System.Windows.Forms.ToolStripButton _tsbMainToolsUnpack;
    }
}

