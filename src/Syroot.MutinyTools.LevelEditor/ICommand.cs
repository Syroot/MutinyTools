﻿using System.Drawing;
using System.Windows.Forms;

namespace Syroot.MutinyTools.LevelEditor
{
    internal interface ICommand
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        Image Image { get; }

        string Name { get; }

        Keys ShortcutKeys { get; }

        bool CanExecute { get; }

        bool IsChecked { get; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        bool Execute();
    }
}
