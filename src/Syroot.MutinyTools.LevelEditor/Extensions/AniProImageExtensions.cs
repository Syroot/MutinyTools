﻿using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Syroot.MutinyTools.Formats;
using MathsColor = Syroot.Maths.Color;

namespace Syroot.MutinyTools.LevelEditor
{
    internal static class AniProImageExtensions
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _tileWidth = 0x10;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static Bitmap ToBitmap(this AniProImage self)
        {
            Bitmap bitmap = new Bitmap(self.Size.X, self.Size.Y, PixelFormat.Format8bppIndexed);
            TransferPalette(self, bitmap);

            // Transfer the bitmap data. Needs to happen row by row as stride is aligned and can differ from width.
            Rectangle bitmapRect = new Rectangle(Point.Empty, bitmap.Size);
            BitmapData bitmapData = bitmap.LockBits(bitmapRect, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
            for (int i = 0; i < self.Size.Y; i++)
            {
                Marshal.Copy(self.Data, i * self.Size.X, bitmapData.Scan0 + i * bitmapData.Stride, self.Size.X);
            }
            bitmap.UnlockBits(bitmapData);

            return bitmap;
        }
        
        internal static void TransferPalette(this AniProImage self, Bitmap bitmap)
        {
            ColorPalette palette = bitmap.Palette;
            for (int i = 0; i < 256; i++)
            {
                MathsColor mathsColor = self.Palette[i];
                palette.Entries[i] = Color.FromArgb(mathsColor.R, mathsColor.G, mathsColor.B);
            }
            bitmap.Palette = palette;
        }
    }
}
