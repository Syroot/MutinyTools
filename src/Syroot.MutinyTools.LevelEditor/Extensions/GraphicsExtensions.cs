﻿using System.Drawing;
using System.Drawing.Imaging;

namespace Syroot.MutinyTools.LevelEditor
{
    /// <summary>
    /// Represents extension methods for the <see cref="Graphics"/> class optimizing drawing operations specific to the
    /// MOTT game.
    /// </summary>
    internal static class GraphicsExtensions
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly PointF[] _destPoints = new PointF[3]; // Cached to reduce GC overhead.
        private static readonly ImageAttributes _attributes;
        private static readonly ImageAttributes _attributesTransparent;
        private static readonly ColorMatrix _opacityMatrix;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        static GraphicsExtensions()
        {
            _opacityMatrix = new ColorMatrix(new float[][]
            {
                new float[] { 1, 0, 0, 0, 0 },
                new float[] { 0, 1, 0, 0, 0 },
                new float[] { 0, 0, 1, 0, 0 },
                new float[] { 0, 0, 0, 1, 0 },
                new float[] { 0, 0, 0, 0, 1 }
            });

            _attributes = new ImageAttributes();
            _attributes.SetColorMatrix(_opacityMatrix);

            _attributesTransparent = new ImageAttributes();
            _attributesTransparent.SetColorKey(GameData.TransparentColor, GameData.TransparentColor);
            _attributesTransparent.SetColorMatrix(_opacityMatrix);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static void DrawImage(this Graphics self, Image image, PointF destination, float opacity)
        {
            SetDestPoints(destination, image.Size);
            RectangleF srcRect = new RectangleF(PointF.Empty, image.Size);
            _opacityMatrix.Matrix33 = opacity;
            _attributes.SetColorMatrix(_opacityMatrix);
            self.DrawImage(image, _destPoints, srcRect, GraphicsUnit.Pixel, _attributes);
        }

        internal static void DrawImageTransparent(this Graphics self, Image image, PointF destination, float opacity)
        {
            SetDestPoints(destination, image.Size);
            RectangleF srcRect = new RectangleF(PointF.Empty, image.Size);
            _opacityMatrix.Matrix33 = opacity;
            _attributesTransparent.SetColorMatrix(_opacityMatrix);
            self.DrawImage(image, _destPoints, srcRect, GraphicsUnit.Pixel, _attributesTransparent);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        internal static void SetDestPoints(PointF location, SizeF size)
        {
            _destPoints[0] = new PointF(location.X, location.Y);
            _destPoints[1] = new PointF(location.X + size.Width, location.Y);
            _destPoints[2] = new PointF(location.X, location.Y + size.Height);
        }
    }
}
