﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MOTT Level Editor")]
[assembly: AssemblyDescription("Level Editor for Mutiny of the Things")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Syroot")]
[assembly: AssemblyProduct("Syroot.MutinyTools.LevelEditor")]
[assembly: AssemblyCopyright("(c) Syroot, licensed under MIT")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("0.1.0.0")]
[assembly: AssemblyFileVersion("0.1.0.0")]
[assembly: ComVisible(false)]
[assembly: Guid("442713a6-616a-437c-a94e-8765b8d7eccb")]

namespace Syroot.MutinyTools.LevelEditor
{
    internal static class AssemblyInfo
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal static string Title
        {
            get
            {
                return Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyTitleAttribute>().Title;
            }
        }
    }
}