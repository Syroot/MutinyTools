﻿using System;
using System.Threading;
using System.Windows.Forms;
using Syroot.MutinyTools.LevelEditor.Main;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    /// <summary>
    /// Represents the main class of the application containing the entry point.
    /// </summary>
    internal static class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [STAThread]
        private static void Main()
        {
            // Catch unhandled exception in a more useful dialog.
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.ThreadException += Application_ThreadException;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ToolStripManager.Renderer = new VisualStudioRenderer();
            Application.Run(new MainWindow());
        }

        private static void ShowUnhandledExceptionMessage(Exception exception)
        {
            if (MessageBox.Show(String.Format("Oops! The editor got an issue it might not fully recover from.{0}"
                + "If you want to help him, please tell the developers about the following:{0}"
                + "\"{1}\"{0}{0}"
                + "Do you want to try to save your work now, even though the editor might behave weirdly? "
                + "Otherwise we will close it for you.", Environment.NewLine, exception.Message),
                AssemblyInfo.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
            {
                Application.Exit();
            }
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ShowUnhandledExceptionMessage(e.Exception);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowUnhandledExceptionMessage((Exception)e.ExceptionObject);
        }
    }
}
