﻿namespace Syroot.MutinyTools.LevelEditor
{
    internal enum LevelLayer
    {
        Background,
        Items,
        Actors,
        Foreground
    }
}
