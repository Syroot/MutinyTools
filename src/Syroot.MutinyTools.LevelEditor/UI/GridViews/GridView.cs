﻿using System;
using System.ComponentModel;
using System.Drawing;
using Syroot.MutinyTools.Formats;
using Syroot.MutinyTools.LevelEditor.UI.Layers;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    public abstract class GridView : AreaView
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private SelectionLayer _selectionLayer;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public override SizeF TotalAreaSize
        {
            get { return new SizeF(GridSize.Width * TilePixels.Width, TilePixels.Height * GridSize.Height); }
        }

        protected abstract Size GridSize { get; }

        protected abstract Size TilePixels { get; }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            if (!DesignMode)
            {
                // Create a layer visualizing the selection.
                _selectionLayer = new SelectionLayer(GridSize, TilePixels);
            }
        }
    }

    internal class SelectionLayer
    {
        internal SelectionLayer(Size size, Size tilePixels)
        {
        }
    }
}
