﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Syroot.MutinyTools.Formats;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    public class TilePickerGridView : GridView
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private TileGridRenderer _tileRenderer;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        internal AniProImage TileImage
        {
            get { return _tileRenderer.TileImage; }
            set
            {
                _tileRenderer.TileImage = value;
                Refresh();
            }
        }
        
        protected override Size GridSize
        {
            get { return GameData.PickerGridSize; }
        }

        protected override Size TilePixels
        {
            get { return GameData.TilePixels; }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            if (!DesignMode)
            {
                _tileRenderer = new TileGridRenderer(GridSize);
                byte[] data = new byte[GridSize.Width * GridSize.Height];
                for (int i = 0; i < data.Length; i++)
                {
                    data[i] = (byte)i;
                }
                _tileRenderer.SetData(data);
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void OnAreaPaint(AreaPaintEventArgs e)
        {
            e.Graphics.InterpolationMode = Zoom < 1 ? InterpolationMode.Bilinear : InterpolationMode.NearestNeighbor;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;
            e.Graphics.DrawImage(_tileRenderer.Render, Point.Empty);
            // Draw the selection layer.
            base.OnAreaPaint(e);
        }
    }
}
