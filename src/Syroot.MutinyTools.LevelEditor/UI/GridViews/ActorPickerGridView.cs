﻿using System.Drawing;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    public class ActorPickerGridView : GridView
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Bitmap _actorBitmap;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal Bitmap ActorBitmap
        {
            get { return _actorBitmap; }
            set
            {
                _actorBitmap = value;
                Refresh();
            }
        }

        protected override Size GridSize
        {
            get { return GameData.ActorImageGridSize; }
        }

        protected override Size TilePixels
        {
            get { return GameData.ActorTilePixels; }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void OnAreaPaint(AreaPaintEventArgs e)
        {
            if (_actorBitmap != null)
            {
                e.Graphics.DrawImage(_actorBitmap, Point.Empty);
            }
            // Draw the selection layer.
            base.OnAreaPaint(e);
        }
    }
}
