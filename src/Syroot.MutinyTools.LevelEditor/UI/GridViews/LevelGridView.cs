﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Syroot.MutinyTools.Formats;
using Syroot.MutinyTools.Level;
using Syroot.MutinyTools.LevelEditor.UI.Layers;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    public class LevelGridView : GridView
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const float _inactiveOpacity = 0.33f;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Color _clearColor;
        private TileGridRenderer _backRenderer;
        private TileGridRenderer _itemRenderer;
        private ActorLayerRenderer _actorRenderer;
        private TileGridRenderer _foreRenderer;

        private LevelLayer _activeLayer;
        private bool _fadeInactiveLayers = true;
        private bool _parallaxVisible = true;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected override Size GridSize
        {
            get { return GameData.MapGridSize; }
        }

        protected override Size TilePixels
        {
            get { return GameData.TilePixels; }
        }

        internal LevelLayer ActiveLayer
        {
            get { return _activeLayer; }
            set
            {
                _activeLayer = value;
                Refresh();
            }
        }

        internal bool FadeInactiveLayers
        {
            get { return _fadeInactiveLayers; }
            set
            {
                _fadeInactiveLayers = value;
                Refresh();
            }
        }

        internal bool ParallaxVisible
        {
            get { return _parallaxVisible; }
            set
            {
                _parallaxVisible = value;
                Refresh();
            }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void SetGameDataAndLevel(GameData gameData, LevelFile level)
        {
            int worldIndex = (int)level.World - 1;

            AniProImage backImage = gameData.BackImages[worldIndex];
            Maths.Color clearColor = backImage.Palette[backImage.Data[0]];
            _clearColor = Color.FromArgb(clearColor.R, clearColor.G, clearColor.B);

            _backRenderer.TileImage = backImage;
            _backRenderer.SetData(level.BackTiles);

            _itemRenderer.TileImage = backImage;
            _itemRenderer.ClearFirstTile = true;
            _itemRenderer.SetData(level.ItemTiles);

            _actorRenderer.ActorBitmap = gameData.ActorBitmap;
            _actorRenderer.SetGameDataAndLevel(gameData, level);

            _foreRenderer.TileImage = gameData.ForeImages[worldIndex];
            _foreRenderer.SetData(level.Tiles);

            Refresh();
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void OnAreaPaint(AreaPaintEventArgs e)
        {
            e.Graphics.InterpolationMode = Zoom < 1 ? InterpolationMode.Bilinear : InterpolationMode.NearestNeighbor;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.Half;
            e.Graphics.Clear(_clearColor);

            if (_parallaxVisible
                && e.Area.Width <= GameData.BackPixelsUsed.Width
                && e.Area.Height <= GameData.BackPixelsUsed.Height)
            {
                // Compute the parallax offset.
                SizeF scrollDelta = new SizeF(
                    e.Area.X / (TotalAreaSize.Width - e.Area.Width),
                    e.Area.Y / (TotalAreaSize.Height - e.Area.Height));
                PointF parallaxOffset = new PointF(
                    scrollDelta.Width * (TotalAreaSize.Width - GameData.BackPixelsUsed.Width),
                    scrollDelta.Height * (TotalAreaSize.Height - GameData.BackPixelsUsed.Height));
                e.Graphics.DrawImage(_backRenderer.Render, parallaxOffset, GetOpacity(LevelLayer.Background));
            }

            // Draw tiles and actors.
            e.Graphics.DrawImageTransparent(_itemRenderer.Render, PointF.Empty, GetOpacity(LevelLayer.Items));
            e.Graphics.DrawImageTransparent(_actorRenderer.Render, PointF.Empty, GetOpacity(LevelLayer.Actors));
            e.Graphics.DrawImageTransparent(_foreRenderer.Render, PointF.Empty, GetOpacity(LevelLayer.Foreground));

            // Draw the selection layer.
            //base.OnAreaPaint(e);
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            if (!DesignMode)
            {
                _backRenderer = new TileGridRenderer(GameData.BackGridSize);
                _itemRenderer = new TileGridRenderer(GridSize);
                _actorRenderer = new ActorLayerRenderer();
                _foreRenderer = new TileGridRenderer(GridSize);
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private float GetOpacity(LevelLayer layer)
        {
            return !FadeInactiveLayers || ActiveLayer == layer ? 1 : _inactiveOpacity;
        }
    }
}
