﻿using System.Drawing;
using System.Windows.Forms;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    public class AreaPaintEventArgs : PaintEventArgs
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public AreaPaintEventArgs(Graphics graphics, Rectangle clipRect, RectangleF area)
            : base(graphics, clipRect)
        {
            Area = area;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public RectangleF Area { get; }
    }
}
