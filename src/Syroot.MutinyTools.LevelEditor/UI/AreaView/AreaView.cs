﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    /// <summary>
    /// Represents a zoomable and pannable view.
    /// </summary>
    public class AreaView : ScrollableControl
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const bool _defaultAllowOutOfBounds = false;
        private const bool _defaultScrollable = true;
        private const bool _defaultZoomable = true;
        private const float _defaultZoom = 1f;
        private const float _defaultZoomMax = 16f;
        private const float _defaultZoomMin = 0.125f;
        private const float _defaultZoomStep = 0.75f;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private PointF _offset;
        private bool _scrollable = _defaultScrollable;
        private float _zoom = _defaultZoom;
        private float _zoomMax = _defaultZoomMax;
        private float _zoomMin = _defaultZoomMin;

        private bool _refreshSuspended;
        private bool _scrollBarUpdateSuspended;
        private bool _isDragging;
        private PointF _dragOffset;
        private Point _dragStart;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="AreaView"/> class.
        /// </summary>
        public AreaView()
        {
            SetStyle(
                ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw,
                true);
            Zoomable = _defaultZoomable;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets a value determining whether the layer view can be moved out of bounds.
        /// </summary>
        [DefaultValue(_defaultAllowOutOfBounds)]
        public bool AllowOutOfBounds { get; set; } = _defaultAllowOutOfBounds;

        /// <summary>
        /// Gets or sets the first coordinate displayed at the top-left corner of the control.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public PointF Offset
        {
            get { return _offset; }
            set
            {
                // Clamp the offset so it does not scroll out of bounds.
                if (!AllowOutOfBounds)
                {
                    SizeF totalLayerSize = TotalAreaSize;
                    SizeF visibleSize = new SizeF(ClientSize.Width / Zoom, ClientSize.Height / Zoom);
                    value.X = Math.Max(0, Math.Min(value.X, totalLayerSize.Width - visibleSize.Width));
                    value.Y = Math.Max(0, Math.Min(value.Y, totalLayerSize.Height - visibleSize.Height));
                }
                _offset = value;
                Refresh();
            }
        }

        /// <summary>
        /// Gets or sets a value determining whether the content can be scrolled.
        /// </summary>
        [DefaultValue(_defaultScrollable)]
        public bool Scrollable
        {
            get { return _scrollable; }
            set
            {
                _scrollable = value;
                UpdateScrollbars();
            }
        }

        /// <summary>
        /// Gets the total area to display.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual SizeF TotalAreaSize { get; }

        /// <summary>
        /// Gets or sets a value determining whether the user can zoom with the mouse wheel.
        /// </summary>
        [DefaultValue(_defaultZoomable)]
        public bool Zoomable { get; set; } = _defaultZoomable;

        /// <summary>
        /// Gets or sets the current zoom level, where 1f is the unscaled original size.
        /// </summary>
        [DefaultValue(_defaultZoom)]
        public float Zoom
        {
            get { return _zoom; }
            set
            {
                _zoom = Math.Max(ZoomMin, Math.Min(value, ZoomMax));
                Refresh();
            }
        }

        /// <summary>
        /// Gets or sets the maximum zoom level, where 1f is the unscaled original size.
        /// </summary>
        [DefaultValue(_defaultZoomMax)]
        public float ZoomMax
        {
            get { return _zoomMax; }
            set
            {
                _zoomMax = value;
                Zoom = Zoom; // Reclamp the current zoom.
            }
        }

        /// <summary>
        /// Gets or sets the minimum zoom level, where 1f is the unscaled original size.
        /// </summary>
        [DefaultValue(_defaultZoomMin)]
        public float ZoomMin
        {
            get { return _zoomMin; }
            set
            {
                _zoomMin = value;
                Zoom = Zoom; // Reclamp the current zoom.
            }
        }

        /// <summary>
        /// Gets or sets the amount of zooming done for one zooms tep.
        /// </summary>
        [DefaultValue(_defaultZoomStep)]
        public float ZoomStep { get; set; }

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Occurs when the mouse clicked the control and the coordinates have been transformed to the visible area.
        /// </summary>
        public event EventHandler<AreaMouseEventArgs> AreaMouseClick;

        /// <summary>
        /// Occurs when the mouse moves above the control and the coordinates have been transformed to the visible area.
        /// </summary>
        public event EventHandler<AreaMouseEventArgs> AreaMouseMove;

        /// <summary>
        /// Occurs when the control has to draw its contents and the coordinates have been transformed to the visible
        /// area.
        /// </summary>
        public event EventHandler<AreaPaintEventArgs> AreaPaint;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the absolute layer coordinate according to the current view offset and zoom of the given
        /// <paramref name="location"/>.
        /// </summary>
        /// <param name="location">The <see cref="Point"/> to convert into an absolute layer coordinate.</param>
        /// <returns>The absolute layer coordinate.</returns>
        public PointF GetPoint(Point location)
        {
            return new PointF(location.X / Zoom + _offset.X, location.Y / Zoom + _offset.Y);
        }

        /// <summary>
        /// Prevents any calls to <see cref="Refresh"/> from actually redrawing the control until
        /// <see cref="ResumeRefresh(bool)"/> is called.
        /// </summary>
        public void SuspendRefresh()
        {
            _refreshSuspended = true;
        }

        /// <summary>
        /// Allows calls to <see cref="Refresh"/> to actually redraw the control again.
        /// </summary>
        /// <param name="immediateRefresh"><c>true</c> to immediately invoke a call to <see cref="Refresh"/>, otherwise
        /// <c>false</c>.</param>
        public void ResumeRefresh(bool immediateRefresh = true)
        {
            _refreshSuspended = false;
            if (immediateRefresh)
            {
                Refresh();
            }
        }

        /// <summary>
        /// Redraws all contents of the control and updates the scrollbars if refreshing has not been suspended earlier.
        /// </summary>
        public override void Refresh()
        {
            if (!_refreshSuspended)
            {
                if (!_scrollBarUpdateSuspended)
                {
                    UpdateScrollbars();
                }
                base.Refresh();
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------
        
        protected virtual void OnAreaMouseClick(AreaMouseEventArgs e)
        {
            AreaMouseClick?.Invoke(this, e);
        }

        protected virtual void OnAreaMouseMove(AreaMouseEventArgs e)
        {
            AreaMouseMove?.Invoke(this, e);
        }
        
        protected virtual void OnAreaPaint(AreaPaintEventArgs e)
        {
            AreaPaint?.Invoke(this, e);
        }

        protected override void OnClientSizeChanged(EventArgs e)
        {
            base.OnClientSizeChanged(e);
            Offset = Offset; // Reclamp the viewport.
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            // Compute the area click location and raise the event.
            OnAreaMouseClick(new AreaMouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta,
                e.Location.X / Zoom - Offset.X, e.Location.Y / Zoom - Offset.Y));
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            // Drag the view with the middle mouse button or Ctrl and left mouse button.
            if (Scrollable && (ModifierKeys == Keys.None && e.Button == MouseButtons.Middle)
                || (ModifierKeys == Keys.Control && e.Button == MouseButtons.Left))
            {
                _isDragging = true;
                _dragStart = e.Location;
                _dragOffset = Offset;
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            // Drag the view if in drag mode.
            if (_isDragging)
            {
                Offset = new PointF(
                    _dragOffset.X - ((e.Location.X - _dragStart.X) / Zoom),
                    _dragOffset.Y - ((e.Location.Y - _dragStart.Y) / Zoom));
            }
            else
            {
                OnAreaMouseMove(new AreaMouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta,
                    e.Location.X / Zoom - Offset.X, e.Location.Y / Zoom - Offset.Y));
            }

            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            // End a drag operation.
            _isDragging = false;

            base.OnMouseUp(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            // Zoom with the mouse wheel if no dragging is performed.
            if (!_isDragging && Zoomable)
            {
                SuspendRefresh();

                // Get the contextual zoom offset (0|0 is top left corner, 1|1 bottom right).
                PointF ctxOffset = new PointF(
                    (float)e.Location.X / ClientSize.Width,
                    (float)e.Location.Y / ClientSize.Height);
                SizeF prevArea = new SizeF(ClientSize.Width / Zoom, ClientSize.Height / Zoom);

                // Perform the zoom.
                if (e.Delta < 0)
                {
                    Zoom *= ZoomStep;
                }
                else
                {
                    Zoom /= ZoomStep;
                }

                // Update the offset for contextual zoom.
                SizeF newArea = new SizeF(ClientSize.Width / Zoom, ClientSize.Height / Zoom);
                Offset = new PointF(
                    Offset.X + (prevArea.Width - newArea.Width) * ctxOffset.X,
                    Offset.Y + (prevArea.Height - newArea.Height) * ctxOffset.Y);

                ResumeRefresh();
            }

            base.OnMouseWheel(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            
            // Compute the visible area and raise the event.
            e.Graphics.ScaleTransform(Zoom, Zoom);
            e.Graphics.TranslateTransform(-Offset.X, -Offset.Y);
            OnAreaPaint(new AreaPaintEventArgs(e.Graphics, e.ClipRectangle,
                new RectangleF(Offset, new SizeF(ClientSize.Width / Zoom, ClientSize.Height / Zoom))));
        }

        protected override void OnScroll(ScrollEventArgs se)
        {
            switch (se.ScrollOrientation)
            {
                case ScrollOrientation.HorizontalScroll:
                    _offset.X = se.NewValue;
                    HorizontalScroll.Value = (int)_offset.X;
                    break;
                case ScrollOrientation.VerticalScroll:
                    _offset.Y = se.NewValue;
                    VerticalScroll.Value = (int)_offset.Y;
                    break;
            }
            _scrollBarUpdateSuspended = true;
            Refresh();
            _scrollBarUpdateSuspended = false;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void UpdateScrollbars()
        {
            // TODO: Implement useful scrollbars for infinite view.
            if (AllowOutOfBounds || !Scrollable)
            {
                HorizontalScroll.Visible = false;
                VerticalScroll.Visible = false;
                return;
            }

            SizeF viewSize = new SizeF(ClientSize.Width / Zoom, ClientSize.Height / Zoom);
            SizeF layerSize = TotalAreaSize;
            SizeF freeTopLeft = new SizeF(-_offset.X, -_offset.Y);
            SizeF freeBotRight = new SizeF(
                viewSize.Width - layerSize.Width - freeTopLeft.Width,
                viewSize.Height - layerSize.Height - freeTopLeft.Height);

            // Horizontal scrollbar.
            if (freeTopLeft.Width < 0 || freeBotRight.Width < 0)
            {
                HorizontalScroll.Visible = true;
                HorizontalScroll.Maximum = (int)layerSize.Width;
                HorizontalScroll.LargeChange = (int)viewSize.Width;
                HorizontalScroll.Value = (int)-freeTopLeft.Width;
            }
            else
            {
                HorizontalScroll.Visible = false;
            }

            // Vertical scrollbar.
            if (freeTopLeft.Height < 0 || freeBotRight.Height < 0)
            {
                VerticalScroll.Visible = true;
                VerticalScroll.Maximum = (int)layerSize.Height;
                VerticalScroll.LargeChange = (int)viewSize.Height;
                VerticalScroll.Value = (int)-freeTopLeft.Height;
            }
            else
            {
                VerticalScroll.Visible = false;
            }
        }
    }
}
