﻿using System.Drawing;
using System.Windows.Forms;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    public class AreaMouseEventArgs : MouseEventArgs
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public AreaMouseEventArgs(MouseButtons button, int clicks, int x, int y, int delta, float areaX, float areaY)
            : base(button, clicks, x, y, delta)
        {
            AreaLocation = new PointF(areaX, areaY);
            AreaX = areaX;
            AreaY = areaY;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public PointF AreaLocation { get; }

        public float AreaX { get; }

        public float AreaY { get; }
    }
}