﻿using System.Drawing;
using System.Windows.Forms;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    /// <summary>
    /// Modern UI-like colors for the ToolStripProfessionalRenderer.
    /// </summary>
    public class VisualStudioColorTable : ProfessionalColorTable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Color _background = Color.FromArgb(255, 255, 255);
        private Color _backgroundDropDown = Color.FromArgb(255, 255, 255);
        private Color _backgroundSelected = Color.FromArgb(242, 214, 214);
        private Color _backgroundPressed = Color.FromArgb(227, 164, 164);
        private Color _backgroundChecked = Color.FromArgb(242, 195, 195);
        private Color _backgroundStatus = Color.FromArgb(154, 44, 44);
        private Color _borderGrip = Color.FromArgb(153, 153, 153);
        private Color _borderDropDown = Color.FromArgb(198, 198, 198);
        private Color _borderSeperatorLight = Color.Transparent;//FromArgb(230, 225, 225);
        private Color _borderSeperatorDark = Color.FromArgb(230, 225, 225);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the color for the ButtonCheckedGradientBegin element.
        /// </summary>
        public override Color ButtonCheckedGradientBegin
        {
            get { return _backgroundChecked; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonCheckedGradientEnd element.
        /// </summary>
        public override Color ButtonCheckedGradientEnd
        {
            get { return _backgroundChecked; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonCheckedGradientMiddle element.
        /// </summary>
        public override Color ButtonCheckedGradientMiddle
        {
            get { return _backgroundChecked; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonCheckedHighlight element.
        /// </summary>
        public override Color ButtonCheckedHighlight
        {
            get { return _backgroundChecked; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonCheckedHighlightBorder element.
        /// </summary>
        public override Color ButtonCheckedHighlightBorder
        {
            get { return Color.Transparent; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonPressedBorder element.
        /// </summary>
        public override Color ButtonPressedBorder
        {
            get { return Color.Transparent; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonPressedGradientBegin element.
        /// </summary>
        public override Color ButtonPressedGradientBegin
        {
            get { return _backgroundPressed; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonPressedGradientEnd element.
        /// </summary>
        public override Color ButtonPressedGradientEnd
        {
            get { return _backgroundPressed; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonPressedGradientMiddle element.
        /// </summary>
        public override Color ButtonPressedGradientMiddle
        {
            get { return _backgroundPressed; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonPressedHighlight element.
        /// </summary>
        public override Color ButtonPressedHighlight
        {
            get { return Color.Transparent; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonPressedHighlightBorder element.
        /// </summary>
        public override Color ButtonPressedHighlightBorder
        {
            get { return Color.Transparent; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonSelectedBorder element.
        /// </summary>
        public override Color ButtonSelectedBorder
        {
            get { return Color.Transparent; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonSelectedGradientBegin element.
        /// </summary>
        public override Color ButtonSelectedGradientBegin
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonSelectedGradientEnd element.
        /// </summary>
        public override Color ButtonSelectedGradientEnd
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonSelectedGradientMiddle element.
        /// </summary>
        public override Color ButtonSelectedGradientMiddle
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonSelectedHighlight element.
        /// </summary>
        public override Color ButtonSelectedHighlight
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the ButtonSelectedHighlightBorder element.
        /// </summary>
        public override Color ButtonSelectedHighlightBorder
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the CheckBackground element.
        /// </summary>
        public override Color CheckBackground
        {
            get { return _backgroundChecked; }
        }

        /// <summary>
        /// Gets or sets the color for the CheckPressedBackground element.
        /// </summary>
        public override Color CheckPressedBackground
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the CheckSelectedBackground element.
        /// </summary>
        public override Color CheckSelectedBackground
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the GripDark element.
        /// </summary>
        public override Color GripDark
        {
            get { return _borderGrip; }
        }

        /// <summary>
        /// Gets or sets the color for the GripLight element.
        /// </summary>
        public override Color GripLight
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the ImageMarginGradientBegin element.
        /// </summary>
        public override Color ImageMarginGradientBegin
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the ImageMarginGradientEnd element.
        /// </summary>
        public override Color ImageMarginGradientEnd
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the ImageMarginGradientMiddle element.
        /// </summary>
        public override Color ImageMarginGradientMiddle
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the ImageMarginRevealedGradientBegin element.
        /// </summary>
        public override Color ImageMarginRevealedGradientBegin
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the ImageMarginRevealedGradientEnd element.
        /// </summary>
        public override Color ImageMarginRevealedGradientEnd
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the ImageMarginRevealedGradientMiddle element.
        /// </summary>
        public override Color ImageMarginRevealedGradientMiddle
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuBorder element.
        /// </summary>
        public override Color MenuBorder
        {
            get { return _borderDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuItemBorder element.
        /// </summary>
        public override Color MenuItemBorder
        {
            get { return Color.Transparent; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuItemPressedGradientBegin element.
        /// </summary>
        public override Color MenuItemPressedGradientBegin
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuItemPressedGradientEnd element.
        /// </summary>
        public override Color MenuItemPressedGradientEnd
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuItemPressedGradientMiddle element.
        /// </summary>
        public override Color MenuItemPressedGradientMiddle
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuItemSelected element.
        /// </summary>
        public override Color MenuItemSelected
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuItemSelectedGradientBegin element.
        /// </summary>
        public override Color MenuItemSelectedGradientBegin
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuItemSelectedGradientEnd element.
        /// </summary>
        public override Color MenuItemSelectedGradientEnd
        {
            get { return _backgroundSelected; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuStripGradientBegin element.
        /// </summary>
        public override Color MenuStripGradientBegin
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the MenuStripGradientEnd element.
        /// </summary>
        public override Color MenuStripGradientEnd
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the OverflowButtonGradientBegin element.
        /// </summary>
        public override Color OverflowButtonGradientBegin
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the OverflowButtonGradientEnd element.
        /// </summary>
        public override Color OverflowButtonGradientEnd
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the OverflowButtonGradientMiddle element.
        /// </summary>
        public override Color OverflowButtonGradientMiddle
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the RaftingContainerGradientBegin element.
        /// </summary>
        public override Color RaftingContainerGradientBegin
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the RaftingContainerGradientEnd element.
        /// </summary>
        public override Color RaftingContainerGradientEnd
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the SeparatorDark element.
        /// </summary>
        public override Color SeparatorDark
        {
            get { return _borderSeperatorDark; }
        }

        /// <summary>
        /// Gets or sets the color for the SeparatorLight element.
        /// </summary>
        public override Color SeparatorLight
        {
            get { return _borderSeperatorLight; }
        }

        /// <summary>
        /// Gets or sets the color for the StatusStripGradientBegin element.
        /// </summary>
        public override Color StatusStripGradientBegin
        {
            get { return _backgroundStatus; }
        }

        /// <summary>
        /// Gets or sets the color for the StatusStripGradientEnd element.
        /// </summary>
        public override Color StatusStripGradientEnd
        {
            get { return _backgroundStatus; }
        }

        /// <summary>
        /// Gets or sets the color for the ToolStripBorder element.
        /// </summary>
        public override Color ToolStripBorder
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the ToolStripContentPanelGradientBegin element.
        /// </summary>
        public override Color ToolStripContentPanelGradientBegin
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the ToolStripContentPanelGradientEnd element.
        /// </summary>
        public override Color ToolStripContentPanelGradientEnd
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the ToolStripDropDownBackground element.
        /// </summary>
        public override Color ToolStripDropDownBackground
        {
            get { return _backgroundDropDown; }
        }

        /// <summary>
        /// Gets or sets the color for the ToolStripGradientBegin element.
        /// </summary>
        public override Color ToolStripGradientBegin
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the ToolStripGradientEnd element.
        /// </summary>
        public override Color ToolStripGradientEnd
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the ToolStripGradientMiddle element.
        /// </summary>
        public override Color ToolStripGradientMiddle
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the ToolStripPanelGradientBegin element.
        /// </summary>
        public override Color ToolStripPanelGradientBegin
        {
            get { return _background; }
        }

        /// <summary>
        /// Gets or sets the color for the ToolStripPanelGradientEnd element.
        /// </summary>
        public override Color ToolStripPanelGradientEnd
        {
            get { return _background; }
        }
    }
}
