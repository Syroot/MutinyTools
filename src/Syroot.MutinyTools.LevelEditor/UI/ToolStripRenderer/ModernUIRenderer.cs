﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    /// <summary>
    /// Renderer for a Modern UI-like interface.
    /// </summary>
    public class VisualStudioRenderer : ToolStripProfessionalRenderer
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualStudioRenderer"/> class.
        /// </summary>
        public VisualStudioRenderer()
            : base(new VisualStudioColorTable())
        {
            RoundedEdges = false;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void OnRenderGrip(ToolStripGripRenderEventArgs e)
        {
            if (e == null) throw new ArgumentNullException("e");

            using (HatchBrush hb = new HatchBrush(HatchStyle.Percent20, ColorTable.GripDark, ColorTable.GripLight))
            {
                Rectangle bounds = e.GripBounds;
                bounds.Inflate(2, -4);
                e.Graphics.FillRectangle(hb, bounds);
            }
        }
    }
}
