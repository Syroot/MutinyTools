﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Syroot.MutinyTools.Formats;

namespace Syroot.MutinyTools.LevelEditor.UI
{
    internal class TileGridRenderer : IDisposable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly Size _tilePixels = GameData.TilePixels;
        private static readonly int _tileBytes = _tilePixels.Width * _tilePixels.Height;
        private static readonly byte[] _tileClearRow = new byte[_tilePixels.Width]; // Cached to reduce GC overhead.

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Size _gridSize;
        private byte[] _data;
        private AniProImage _tileImage;
        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal TileGridRenderer(Size gridSize)
        {
            _gridSize = gridSize;
            _data = new byte[_gridSize.Width * _gridSize.Height];
            Render = new Bitmap(_gridSize.Width * _tilePixels.Width, _gridSize.Height * _tilePixels.Height,
                PixelFormat.Format8bppIndexed);
        }
        
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal AniProImage TileImage
        {
            get { return _tileImage; }
            set
            {
                _tileImage = value;
                _tileImage.TransferPalette(Render);
                UpdateRender(new Rectangle(Point.Empty, _gridSize));
            }
        }

        internal Bitmap Render { get; private set; }

        internal bool ClearFirstTile { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------
        
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void SetData(byte[] data)
        {
            SetData(data, new Rectangle(Point.Empty, _gridSize));
        }

        internal void SetData(byte[] data, Rectangle area)
        {
            if (data.Length != area.Width * area.Height)
                throw new ArgumentException("Updateable area does not result in data length.", nameof(area));

            for (int y = 0; y < area.Height; y++)
            {
                Buffer.BlockCopy(data, y * area.Width, _data, area.X + ((area.Y + y) * area.Width), area.Width);
            }
            UpdateRender(area);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Render?.Dispose();
                }
                _disposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------
        
        private void UpdateRender(Rectangle dirty)
        {
            if (TileImage == null || _data == null) return;

            // Retrieve raw image data access on the region which requires an update.
            Rectangle rect = new Rectangle(dirty.X * _tilePixels.Width, dirty.Y * _tilePixels.Height,
                dirty.Width * _tilePixels.Width, dirty.Height * _tilePixels.Height);
            BitmapData bitmapData = Render.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            // Update the tiles in that region.
            int tileRowStride = bitmapData.Stride * _tilePixels.Height;
            for (int y = dirty.Top; y < dirty.Bottom; y++)
            {
                int dstY = y - dirty.Top;
                int yDataOffset = y * _gridSize.Width;
                for (int x = dirty.Left; x < dirty.Right; x++)
                {
                    // Split the sequential tile image data into tile rows and write those to the destination.
                    int dstX = x - dirty.Left;
                    int dstByte = dstX * _tilePixels.Width + dstY * tileRowStride;
                    byte data = _data[x + yDataOffset];
                    if (ClearFirstTile && data == 0)
                    {
                        for (int i = 0; i < _tilePixels.Height; i++)
                        {
                            Marshal.Copy(_tileClearRow, 0, bitmapData.Scan0 + dstByte, _tilePixels.Width);
                            dstByte += bitmapData.Stride;
                        }
                    }
                    else
                    {
                        int srcFirstByte = data * _tileBytes;
                        int srcLastByte = srcFirstByte + _tileBytes;
                        for (int srcByte = srcFirstByte; srcByte < srcLastByte; srcByte += _tilePixels.Width)
                        {
                            Marshal.Copy(TileImage.Data, srcByte, bitmapData.Scan0 + dstByte, _tilePixels.Width);
                            dstByte += bitmapData.Stride;
                        }
                    }
                }
            }

            Render.UnlockBits(bitmapData);
        }
    }
}