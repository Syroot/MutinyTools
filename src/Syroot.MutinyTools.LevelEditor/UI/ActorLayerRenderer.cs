﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Syroot.MutinyTools.Level;

namespace Syroot.MutinyTools.LevelEditor.UI.Layers
{
    /// <summary>
    /// Represents freely placed actors in a MOTT level.
    /// </summary>
    internal class ActorLayerRenderer : IDisposable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly Array _actorTypes = Enum.GetValues(typeof(ActorType));

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Graphics _graphics;
        private GameData _gameData;
        private LevelFile _level;
        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal ActorLayerRenderer()
        {
            Render = new Bitmap(
                GameData.MapGridSize.Width * GameData.TilePixels.Width,
                GameData.MapGridSize.Height * GameData.TilePixels.Height);
            _graphics = Graphics.FromImage(Render);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal Bitmap ActorBitmap { get; set; }

        internal Bitmap Render { get; private set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void SetGameDataAndLevel(GameData gameData, LevelFile level)
        {
            _gameData = gameData;
            _level = level;
            UpdateRender();
        }

        internal void UpdateRender()
        {
            if (ActorBitmap == null || _gameData == null || _level == null) return;

            // Draw all actors.
            foreach (ActorType actorType in _actorTypes)
            {
                foreach (IActor actor in GetActorsByType(actorType))
                {
                    if (actor.X + actor.Y > 0)
                    {
                        Bitmap bitmap;
                        if (actorType == ActorType.Elevator)
                        {
                            bitmap = _gameData.ElevatorBitmaps[(int)_level.World - 1];
                        }
                        else
                        {
                            bitmap = _gameData.ActorBitmaps[(int)actorType];
                        }
                        // Draw mirrored if direction points right.
                        int mirrorSign = actor.Direction == SpriteDirection.RightOrDown ? -1 : 1;
                        _graphics.ScaleTransform(mirrorSign, 1);
                        _graphics.TranslateTransform(mirrorSign * actor.X, actor.Y - bitmap.Height);
                        _graphics.DrawImageTransparent(bitmap, PointF.Empty, 1);
                        _graphics.ResetTransform();
                    }
                }
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Render?.Dispose();
                }
                _disposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private IActor[] GetActorsByType(ActorType actorType)
        {
            switch (actorType)
            {
                case ActorType.Player:
                    return new IActor[] { _level.Player };
                case ActorType.Apple:
                    return Array.ConvertAll(_level.Apples, x => (IActor)x);
                case ActorType.Carrot:
                    return Array.ConvertAll(_level.Carrots, x => (IActor)x);
                case ActorType.Daisy:
                    return Array.ConvertAll(_level.Daisies, x => (IActor)x);
                case ActorType.Chopper:
                    return Array.ConvertAll(_level.Choppers, x => (IActor)x);
                case ActorType.Shoe:
                    return Array.ConvertAll(_level.Shoes, x => (IActor)x);
                case ActorType.Pencil:
                    return Array.ConvertAll(_level.Pencils, x => (IActor)x);
                case ActorType.Ooze:
                    return Array.ConvertAll(_level.Oozes, x => (IActor)x);
                case ActorType.Tank:
                    return Array.ConvertAll(_level.Tanks, x => (IActor)x);
                case ActorType.Snowman:
                    return Array.ConvertAll(_level.Snowmen, x => (IActor)x);
                case ActorType.IceCone:
                    return Array.ConvertAll(_level.IceCones, x => (IActor)x);
                case ActorType.FlatIron:
                    return Array.ConvertAll(_level.FlatIrons, x => (IActor)x);
                case ActorType.Lightbulb:
                    return Array.ConvertAll(_level.Lightbulbs, x => (IActor)x);
                case ActorType.Elevator:
                    return Array.ConvertAll(_level.Elevators, x => (IActor)x);
                case ActorType.Stomper:
                    return Array.ConvertAll(_level.Stompers, x => (IActor)x);
                default:
                    throw new ArgumentException($"Invalid actor type {actorType}.", nameof(actorType));
            }
        }
    }
}
