﻿using System.Drawing;
using System.IO;
using Syroot.MutinyTools.Formats;

namespace Syroot.MutinyTools.LevelEditor
{
    /// <summary>
    /// Represents relevant contents loaded from an unpacked game data directory.
    /// </summary>
    internal class GameData
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        internal static readonly Size TilePixels = new Size(16, 16);
        internal static readonly Size TileImageGridSize = new Size(1, 256);

        internal static readonly Size PickerGridSize = new Size(20, 12);

        internal static readonly Size BackGridSize = new Size(0x40, 0x30);
        internal static readonly Size BackGridSizeUsed = new Size(46, 32);
        internal static readonly Size BackPixelsUsed = new Size(
            BackGridSizeUsed.Width * TilePixels.Width, BackGridSizeUsed.Height * TilePixels.Height);

        internal static readonly Size MapGridSize = new Size(0x80, 0x60);

        internal static readonly Size ActorTilePixels = new Size(32, 50);
        internal static readonly Size ActorImageGridSize = new Size(10, 2);

        internal static Color TransparentColor = Color.FromArgb(0, 0, 4);

        private static readonly string[] _worldNames = new[] { "FAB", "NAT", "EIS", "SPA" };
        private static readonly string[] _actorBitmapNames = new[]
        {
            "HELDPIC",
            "APFEPIC",
            "KAROPIC",
            "GAENPIC",
            "GEBIPIC",
            "SCHUPIC",
            "BLEIPIC",
            "SCHLPIC",
            "PANZPIC",
            "SCHNPIC",
            "EISPIC",
            "EISEPIC",
            "BIRNPIC",
            null, // Retrieve file name via Level world.
            "STAMPFER"
        };

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private string _directoryPath;
        
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal string DirectoryPath
        {
            get { return _directoryPath; }
            set
            {
                _directoryPath = value;
                LoadData();
            }
        }

        internal AniProImage[] BackImages { get; private set; }

        internal AniProImage[] ForeImages { get; private set; }

        internal Bitmap[] ElevatorBitmaps { get; private set; }

        internal Bitmap ActorBitmap { get; private set; }
        
        internal Bitmap[] ActorBitmaps { get; private set; }
        
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void LoadData()
        {
            // Load world-dependent images and bitmaps.
            int i = 0;
            BackImages = new AniProImage[_worldNames.Length];
            ForeImages = new AniProImage[_worldNames.Length];
            ElevatorBitmaps = new Bitmap[_worldNames.Length];
            foreach (string worldName in _worldNames)
            {
                BackImages[i] = LoadImage($"HIN-{worldName}.PIC");
                ForeImages[i] = LoadImage($"FOR-{worldName}.PIC");
                ElevatorBitmaps[i] = LoadBitmap($"FA-{worldName}.CEL");
                i++;
            }

            // Load actor bitmaps.
            ActorBitmap = LoadBitmap("WESEN.PIC");
            i = 0;
            ActorBitmaps = new Bitmap[_actorBitmapNames.Length];
            foreach (string actorBitmapName in _actorBitmapNames)
            {
                if (actorBitmapName != null)
                {
                    ActorBitmaps[i] = LoadBitmap(actorBitmapName + ".CEL");
                }
                i++;
            }
        }

        private AniProImage LoadImage(string fileName)
        {
            return new AniProImage(Path.Combine(_directoryPath, fileName));
        }

        private Bitmap LoadBitmap(string fileName)
        {
            return new AniProImage(Path.Combine(_directoryPath, fileName)).ToBitmap();
        }
    }
}
