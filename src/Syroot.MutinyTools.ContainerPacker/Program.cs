﻿using System;
using System.Collections.Generic;
using System.IO;
using Syroot.MutinyTools.Container;

namespace Syroot.MutinyTools.Test
{
    /// <summary>
    /// Represents the main class of the application containing the program entry point.
    /// </summary>
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void Main(string[] args)
        {
            // Check if either a file or directory has been passed.
            string path = args.Length == 1 ? args[0] : null;
            if (String.IsNullOrEmpty(path))
            {
                WriteUsage();
            }
            else if (Directory.Exists(path))
            {
                PackContainer(path);
            }
            else if (File.Exists(path))
            {
                UnpackContainer(path);
            }
            else
            {
                Console.WriteLine($"File or directory \"{path}\" does not exist.");
            }
        }
        
        private static void WriteUsage()
        {
            Console.WriteLine("Packs or unpacks CAPS data containers.");
            Console.WriteLine();
            Console.WriteLine("CONTAINERPACKER FILE|FOLDER");
            Console.WriteLine();
            Console.WriteLine("        FILE   	The name of a data container file to unpack into a");
            Console.WriteLine("                 directory of the same name.");
            Console.WriteLine("        FOLDER   The name of a directory which contents will be stored");
            Console.WriteLine("                 in a CAPS data container of the same name.");
        }

        private static void PackContainer(string directoryName)
        {
            string outputFile = Path.ChangeExtension(directoryName, "CNT");
            Console.WriteLine($"Packing \"{Path.GetFileName(directoryName)}\" to \"{outputFile}\"...");

            // Create the container and pack all files into it.
            using (ContainerFile container = new ContainerFile())
            {
                foreach (string file in Directory.GetFiles(directoryName))
                {
                    string fileName = Path.GetFileName(file);
                    Console.WriteLine($"Adding {fileName}");
                    container.Entries.Add(fileName, new FileContainerEntry(file));
                }
                // Save the container.
                Console.Write($"Saving container... ");
                container.Save(outputFile);
            }
            Console.WriteLine("OK");
        }

        private static void UnpackContainer(string fileName)
        {
            string outputDirectory = Path.ChangeExtension(fileName, null);
            Console.WriteLine($"Unpacking \"{Path.GetFileName(fileName)}\" to \"{outputDirectory}\"...");

            // Load the container.
            using (ContainerFile container = new ContainerFile(fileName))
            {
                // Create the directory.
                Directory.CreateDirectory(outputDirectory);

                // Unpack all files into it.
                foreach (KeyValuePair<string, IContainerEntry> entry in container.Entries)
                {
                    Console.Write($"Unpacking {entry.Key}... ");
                    string outputFile = Path.Combine(outputDirectory, entry.Key);
                    using (FileStream stream
                        = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        entry.Value.CopyDataTo(stream);
                    }
                    Console.WriteLine("OK");
                }
            }

            Console.WriteLine("Done.");
        }
    }
}