# MutinyTools

This repository provides a .NET library (written in C#) which is capable of loading, modifying and saving game data of
the 1994 DOS Jump'n'Run game "Jack Flash: Mutiny of the Things" ("DADD: Der Aufstand der Dinge" in German), created by CAPS Software.

## Why exactly _this_ game?

Even if MOTT is not one of the popular boys in the hood, it was one of the very first computer games I ever played.
Basically since my childhood I had interest in creating new levels and modifying its contents, sketching out several
"worlds" back then on paper. After 20 years, I finally decided to create real tools to make this a reality.

I was able to contact the original author (Christian Aschoff) some years ago, and he sent me a collection of old
floppies on which we assumed to find the original Pascal source code of the game. While it contained most parts of the game engine (of slightly older dates) and code of other games utilizing it, this game's specifc source was not found, so the work put into this library is based on reverse engineering the game and its formats.

## Available Tools

- **ContainerPacker** is a command line tool to pack or unpack game data containers.
- **MOTT Level Editor** can display and later on edit levels.
![MOTT Level Editor](https://gitlab.com/Syroot/MutinyTools/raw/master/doc/readme/level_editor.png)